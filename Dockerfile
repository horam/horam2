FROM ubuntu:20.04 AS horam2-base
RUN apt-get update && \
    DEBIAN_FRONTEND=noninteractive apt-get install -yq \
        build-essential \
        python3-dev \
        python3-pip \
        locales \
        libpq-dev \
        libtiff5-dev \
        libjpeg8-dev \
        libfreetype6-dev \
        liblcms2-dev \
        libwebp-dev \
        libssl-dev \
        && \
    locale-gen sk_SK.UTF-8 && \
    update-locale
ENV LANG sk_SK.utf8
ENV LC_ALL sk_SK.utf8
RUN pip3 install pipenv

FROM horam2-base
COPY ./docker/manage.sh /usr/local/bin/manage
RUN ln -s /usr/local/bin/manage /usr/local/bin/manage.py
COPY ./docker/wait-for-postgres.py /wait-for-postgres.py
COPY . /app
RUN mkdir /uploads
WORKDIR /app
RUN pipenv sync
STOPSIGNAL SIGINT
CMD ["pipenv", "run", \
     "python", "-u", "/wait-for-postgres.py", "host=postgres port=5432 user=horam dbname=horam", "--", \
     "python", "-u", "manage.py", "runserver", "0:8080"]
