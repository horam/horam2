Horam web page v2
-----------------


Developing with Docker compose
==============================

1. Install git

   * Debian/Ubuntu/Linux Mint/...: `sudo apt install git`
   * Windows: [installer](https://git-scm.com/download/win)
     you then run git bash

2. Disable line-ending conversion (Windows only):

   ```
   git config --global core.autocrlf input
   ```

   For Windows, you need to disable line-ending conversion, otherwise
   git will change line endings to Windows format, which breaks the
   bash scripts when running in Docker.

3. Checkout the source code

   ```
   git clone https://gitlab.com/horam/horam2.git
   ```

   Note: You need to checkout to a directory where Docker can write
   files, a subdirectory under Documents worked for me, but a root of
   a disk (like `D:`) did not.

4. Install [Docker](https://docs.docker.com/install/) and
   [Docker compose](https://docs.docker.com/compose/install/)

   Note: For older Windows and Windows 10 Home, you need
   [Docker Toolbox](https://docs.docker.com/toolbox/overview/) instead
   of Docker for Desktop as the latter requires Hyper-V.

5. Generate secrets for your dev environment

    ```bash
    ./tools/init-secrets.sh
    # secrets are stored in .dev-secrets directory
    ```

6. Start up containers

    ```
    docker-compose up
    ```
    
    Note: In Linux you might need to run docker-compose commands with sudo.

7. Initialize a new database with

    ```bash
    docker-compose exec web manage migrate
    ```

8. Open http://localhost:8080 in browser

    Note: In Windows, Docker Toolbox does not map ports to localhost,
    you might need to access the NATed IP address directly. For this,
    it is necessary to add it to [ALLOWED_HOSTS](https://docs.djangoproject.com/en/2.1/ref/settings/#allowed-hosts)
    setting in horam/settings_dev.py
    

To open a psql shell:

```bash
docker-compose exec postgres psql -U horam
```

To open a web bash shell with python env set up:

```bash
docker-compose exec web pipenv run bash
```

To run tests

```bash
docker-compose exec web manage test
```


Installing on a machine manually
================================

1. Install PostgreSQL: `sudo apt install postgresql`
2. Ensure `sk_SK.UTF-8` locale is present:

    ```
    sudo locale -a  # check the list
    # To add if not present:
    locale-gen sk_SK.UTF-8
    update-locale
    ```
    
3. Create a user for app (optional):

    ```
    sudo adduser --system horam
    sudo -u postgres psql
    create user horam;
    ```

4. Create database:

    ```
    sudo -u postgres psql
    create database horam with owner=horam encoding='UTF8' lc_collate='sk_SK.UTF-8' lc_ctype='sk_SK.UTF-8' template=template0;
    ```

5. Create virtualenv for app:

    ```
    # database deps
    sudo apt install python3-pip virtualenv libpq-dev
    # pillow deps
    sudo apt install libtiff5-dev libjpeg8-dev libfreetype6-dev liblcms2-dev libwebp-dev
    # install pipenv
    sudo pip install pipenv
    sudo -u horam -s
    pipenv sync
    ```

6. Run migrations

    ```bash
    pipenv run manage.py migrate
    ```

7. Run app:

    ```
    . venv/bin/activate
    pipenv run manage.py runserver
    ```

8. Serve app using nginx/gunicorn

    ```
    sudo apt install nginx
    ```
