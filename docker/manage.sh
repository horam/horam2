#!/usr/bin/env bash
exec pipenv run python -u /app/manage.py "$@"