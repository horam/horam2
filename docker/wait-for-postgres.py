#!/usr/bin/env python3
import argparse
import psycopg2
import time
import os
import logging

parser = argparse.ArgumentParser()
parser.add_argument('dsn')
parser.add_argument('command', nargs='*')

args = parser.parse_args()

with open(os.environ['POSTGRES_PASSWORD_FILE'], 'rb') as f:
    password = f.read().decode('utf-8')

logging.basicConfig(level=logging.DEBUG, format='[%(asctime)-15s] %(message)s')
logger = logging.getLogger()

while True:
    try:
        with psycopg2.connect(args.dsn, password=password) as conn:
            with conn.cursor() as cur:
                cur.execute('select 1')
    except psycopg2.Error as e:
        logger.error(e)
        logger.info('Waiting for postgres to be ready')
        time.sleep(1)
    else:
        break

logger.info('Postgres is ready')

if args.command:
    logger.info('Running %s', repr(args.command))
    os.execvp(args.command[0], args.command)
