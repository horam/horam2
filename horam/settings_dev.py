# noinspection PyUnresolvedReferences
from horam.settings_base import *


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/2.1/howto/deployment/checklist/


def get_secret(envvar):
    """Get a secret from file"""
    with open(os.getenv(envvar), 'rb') as f:
        return f.read().decode('utf-8')


# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = get_secret('SECRET_KEY_FILE')

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

# Database
# https://docs.djangoproject.com/en/2.1/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'USER': 'horam',
        'PASSWORD': get_secret('POSTGRES_PASSWORD_FILE'),
        'NAME': 'horam',
        'HOST': 'postgres',
        'TEST': {
            'NAME': 'testhoram',
        },
    },
}

# Show e-mails in console
EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'


def show_toolbar(request):
    return DEBUG


DEBUG_TOOLBAR_CONFIG = {
    'SHOW_TOOLBAR_CALLBACK': 'horam.settings_dev.show_toolbar',
}

MEDIA_ROOT = '/app/uploads'
