import re

from django.contrib.staticfiles.storage import ManifestStaticFilesStorage


class HoramManifestStaticFilesStorage(ManifestStaticFilesStorage):
    def hashed_name(self, name, content=None, filename=None):
        if re.search('(^|/)vendor/', name):
            # don't hash vendor files
            return name
        return super().hashed_name(name, content, filename)
