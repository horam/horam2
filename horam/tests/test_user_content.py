from unittest import TestCase

import mistune

from horam.web.user_content import HoramMarkdownRenderer, render_user_content, \
    LegacyUserContent, MarkdownUserContent


class TestHoramMarkdown(TestCase):
    def _render(self, text, **kwargs):
        renderer = HoramMarkdownRenderer(**kwargs)
        return mistune.markdown(text, renderer=renderer)

    def test_image(self):
        html = self._render('![alt text](https://server/image.png)')

        self.assertEqual('<p><img src="https://server/image.png" alt="alt text"></p>\n', html)

    def test_image_xhtml(self):
        html = self._render('![alt text](https://server/image.png)', use_xhtml=True)

        self.assertEqual('<p><img src="https://server/image.png" alt="alt text" /></p>\n', html)

    def test_image_with_title(self):
        html = self._render('![alt text](https://server/image.png "title text")')

        self.assertEqual('<p><figure><img src="https://server/image.png" alt="alt text" title="title text">'
                         '<figcaption>title text</figcaption></figure></p>\n', html)

    def test_map(self):
        html = self._render('![alt text](map:///47.5,16.1)')

        self.assertEqual('<p><div class="map" data-lon="16.1" data-lat="47.5"></div></p>\n', html)

    def test_map_invalid_location(self):
        html = self._render('![alt text](map:///47.5.16.1)')

        self.assertEqual('<p>(invalid map location)</p>\n', html)

    def test_map_invalid_url(self):
        html = self._render('![alt text](map://[/)')

        self.assertEqual('<p>(invalid map url)</p>\n', html)

    def test_map_with_title(self):
        html = self._render('![alt text](map:///47.5,16.1 "title text")')

        self.assertEqual('<p><figure><div class="map" data-lon="16.1" data-lat="47.5"></div>'
                         '<figcaption>title text</figcaption></figure></p>\n', html)


class TestUserContent(TestCase):
    def test_legacy_to_html(self):
        text_in = 'Test  text\n' \
                  '[url=https://example.com]Link[/url]\n' \
                  '[img]https://example.com/img.png[/img]\n' \
                  '[map]47.12345,16.12345[/map]\n' \
                  '[b]bold[/b]\n' \
                  '[i]italic[/i]\n' \
                  '[u]underline[/u]\n' \
                  '[b][i][u]combined[/u][/i][/b]\n' \
                  '[h1]header1[/h1]\n' \
                  '[h2]header2[/h2]\n' \
                  '[h3]header3[/h3]\n' \
                  '[center]center[/center]'

        html = LegacyUserContent(text_in).render_html()

        expected = 'Test\xa0\xa0text<br>' \
                   '<a href="https://example.com">Link</a><br>' \
                   '<img src="https://example.com/img.png"><br>' \
                   '<div class="map" data-lat="47.12345" data-lon="16.12345"></div><br>' \
                   '<b>bold</b><br>' \
                   '<i>italic</i><br>' \
                   '<u>underline</u><br>' \
                   '<b><i><u>combined</u></i></b><br>' \
                   '<h1>header1</h1><br>' \
                   '<h2>header2</h2><br>' \
                   '<h3>header3</h3><br>' \
                   '<center>center</center>'

        self.assertEqual(expected, html)

    def test_markdown_to_html(self):
        text_in = 'Test text\n' \
                  '<script>alert("pwned");</script>\n\n' \
                  '![](https://example.com)\n' \
                  '# Header\n' \
                  '![alt text](map:///47.5,16.1 "title text")'

        html = MarkdownUserContent(text_in).render_html()

        expected = '<p>Test text</p>\n' \
                   '&lt;script&gt;alert("pwned");&lt;/script&gt;' \
                   '<p><img alt="" src="https://example.com"></p>\n' \
                   '<h1>Header</h1>\n' \
                   '<figure><div class="map" data-lat="47.5" data-lon="16.1"></div>' \
                   '<figcaption>title text</figcaption></figure>\n'

        self.assertEqual(expected, html)

    def test_render_user_content_oldhoram(self):
        html = render_user_content('oldhoram\r\n[h1]test[/h1]')
        expected = '<h1>test</h1>'

        self.assertEqual(expected, html)

    def test_render_user_content_markdown(self):
        html = render_user_content('# test')
        expected = '<h1>test</h1>\n'

        self.assertEqual(expected, html)
