from django.core.exceptions import ObjectDoesNotExist
from django.utils.html import escape, format_html, format_html_join

from horam.web.models import User, ActivityType, ActivityLinkType, Sex, ExtendedParticipationState


class Text:
    def __init__(self, text):
        self.text = text

    def as_text(self):
        return self.text

    def as_html(self):
        return escape(self.text)


class Link:
    def __init__(self, text, url):
        self.text = text
        self.url = url

    def as_text(self):
        return self.text

    def as_html(self):
        return format_html('<a href="{url}">{text}</a>', url=self.url, text=self.text)


class ActivityFormatter:
    def __init__(self, url_transform=None):
        self.url_transform = url_transform

    def _url(self, url):
        if self.url_transform:
            return self.url_transform(url)
        return url

    def _person_link(self, person):
        try:
            return Link(person.nickname, self._url(person.get_absolute_url()))
        except (User.DoesNotExist, AttributeError):
            return Text(person.nickname)

    def _activity_link(self, activity_link, text=None, url_suffix=''):
        if activity_link is None:
            if text is not None:
                return Text(text)
            raise ValueError('Empty link')

        if text is None:
            text = activity_link.linked_name

        try:
            return Link(text, self._url(activity_link.linked_object.get_absolute_url()) + url_suffix)
        except (ObjectDoesNotExist, AttributeError):
            return Text(text)

    def tokens(self, activity):
        """Generate tokens of sentences describing the activity"""
        actor_token = self._person_link(activity.actor)

        def sex(sex, male_text, female_text):
            if sex == Sex.MALE:
                return male_text
            elif sex == Sex.FEMALE:
                return female_text
            raise ValueError('Unknown sex')

        def action_lookup(d):
            """Lookup a verb in d based on action_type and sex.

            :param d: Dictionary of ActivityType → (male_text, female_text)
            """
            return Text(sex(activity.actor.sex, *d[activity.action_type]))

        if activity.action_type in (ActivityType.ADD_COMMENT, ActivityType.UPDATE_COMMENT, ActivityType.DELETE_COMMENT):
            action_token = action_lookup({
                ActivityType.ADD_COMMENT: ('pridal', 'pridala'),
                ActivityType.UPDATE_COMMENT: ('upravil', 'upravila'),
                ActivityType.DELETE_COMMENT: ('zmazal', 'zmazala'),
            })

            target_info = {
                ActivityLinkType.EVENT: 'k akcii',
                ActivityLinkType.TOPIC: 'k téme',
            }[activity.target_link.linked_type]

            yield actor_token
            yield action_token

            try:
                if activity.object_link.comment_id:
                    yield self._activity_link(activity.target_link,
                                              text='komentár',
                                              url_suffix='#komentar-{}'.format(activity.object_link.comment_id))
                else:
                    raise ObjectDoesNotExist()
            except (ObjectDoesNotExist, AttributeError):
                yield Text('komentár')

            yield Text(target_info)
            yield self._activity_link(activity.target_link)

        elif activity.action_type == ActivityType.UPDATE_PAGE:
            action_token = action_lookup({
                ActivityType.UPDATE_PAGE: ('upravil', 'upravila'),
            })

            try:
                url = '/page/view/' + activity.details['page_slug']
            except KeyError:  # no page_slug
                object_token = Text(activity.object_link.linked_name)
            else:
                object_token = Link(activity.object_link.linked_name, self._url(url))

            yield actor_token
            yield action_token
            yield Text('stránku')
            yield object_token

        elif activity.action_type in (ActivityType.ADD_EVENT, ActivityType.UPDATE_EVENT, ActivityType.DELETE_EVENT):
            action_token = action_lookup({
                ActivityType.ADD_EVENT: ('pridal', 'pridala'),
                ActivityType.UPDATE_EVENT: ('upravil', 'upravila'),
                ActivityType.DELETE_EVENT: ('zmazal', 'zmazala'),
            })

            yield actor_token
            yield action_token
            yield Text('akciu')
            yield self._activity_link(activity.object_link)

        elif activity.action_type == ActivityType.UPDATE_PARTICIPATION_STATE:
            action_token = action_lookup({
                ActivityType.UPDATE_PARTICIPATION_STATE: ('zmenil', 'zmenila'),
            })

            yield actor_token
            yield action_token
            yield Text('svoju účasť na akcii')
            yield self._activity_link(activity.target_link)
            yield Text('na „{state}“'.format(state=ExtendedParticipationState(activity.details['state']).label))

        elif activity.action_type in (ActivityType.ADD_REVIEW, ActivityType.UPDATE_REVIEW):
            action_token = action_lookup({
                ActivityType.ADD_REVIEW: ('pridal', 'pridala'),
                ActivityType.UPDATE_REVIEW: ('upravil', 'upravila'),
            })

            yield actor_token
            yield action_token
            yield Text('záznam pre akciu')
            yield self._activity_link(activity.target_link)

        elif activity.action_type in (ActivityType.ADD_TOPIC, ActivityType.UPDATE_TOPIC, ActivityType.DELETE_TOPIC):
            action_token = action_lookup({
                ActivityType.ADD_TOPIC: ('pridal', 'pridala'),
                ActivityType.UPDATE_TOPIC: ('upravil', 'upravila'),
                ActivityType.DELETE_TOPIC: ('zmazal', 'zmazala'),
            })

            yield actor_token
            yield action_token
            yield Text('tému')
            yield self._activity_link(activity.object_link)

        elif activity.action_type in (ActivityType.ADD_USER, ActivityType.DELETE_USER, ActivityType.ACTIVATE_USER,
                                      ActivityType.DEACTIVATE_USER):
            action_token = action_lookup({
                ActivityType.ADD_USER: ('pridal', 'pridala'),
                ActivityType.DELETE_USER: ('zmazal', 'zmazala'),
                ActivityType.ACTIVATE_USER: ('aktivoval', 'aktivovala'),
                ActivityType.DEACTIVATE_USER: ('deaktivoval', 'deaktivovala'),
            })

            yield actor_token
            yield action_token
            yield Text('používateľa')
            yield self._activity_link(activity.object_link)

        elif activity.action_type in (ActivityType.ADD_PHOTO, ActivityType.UPDATE_PHOTO):
            action_token = action_lookup({
                ActivityType.ADD_PHOTO: ('pridal', 'pridala'),
                ActivityType.UPDATE_PHOTO: ('upravil', 'upravila'),
            })

            yield actor_token
            yield action_token
            yield Text('fotku')

            if activity.target_link.linked_type == ActivityLinkType.PERSON:
                if activity.target_link.linked_object_id == activity.actor.id:
                    yield Text({
                        ActivityType.ADD_PHOTO: 'do svojho profilu',
                        ActivityType.UPDATE_PHOTO: 'vo svojom profile',
                    }[activity.action_type])
                else:
                    yield Text({
                        ActivityType.ADD_PHOTO: 'do profilu',
                        ActivityType.UPDATE_PHOTO: 'v profile',
                    }[activity.action_type])

                    yield self._activity_link(activity.target_link)
            elif activity.target_link.linked_type == ActivityLinkType.EVENT:
                yield Text('k akcii')
                yield self._activity_link(activity.target_link)
            else:
                raise ValueError('Unexpected target for photo activity')
        elif activity.action_type in (ActivityType.ADD_BLOG_ARTICLE, ActivityType.UPDATE_BLOG_ARTICLE):
            action_token = action_lookup({
                ActivityType.ADD_BLOG_ARTICLE: ('pridal', 'pridala'),
                ActivityType.UPDATE_BLOG_ARTICLE: ('upravil', 'upravila'),
            })

            yield actor_token
            yield action_token
            yield Text('novinku')
            yield self._activity_link(activity.object_link)
        elif activity.action_type in (ActivityType.ADD_MATERIAL_MODEL, ActivityType.UPDATE_MATERIAL_MODEL):
            action_token = action_lookup({
                ActivityType.ADD_MATERIAL_MODEL: ('pridal', 'pridala'),
                ActivityType.UPDATE_MATERIAL_MODEL: ('upravil', 'upravila'),
            })

            yield actor_token
            yield action_token
            yield Text('model materiálu')
            yield self._activity_link(activity.object_link)
        elif activity.action_type in (ActivityType.COMMISSION_MATERIAL_ITEM, ActivityType.DECOMMISSION_MATERIAL_ITEM):
            action_token = action_lookup({
                ActivityType.COMMISSION_MATERIAL_ITEM: ('zaradil', 'zaradila'),
                ActivityType.DECOMMISSION_MATERIAL_ITEM: ('vyradil', 'vyradila'),
            })

            yield actor_token
            yield action_token

            count = activity.details['count']
            if count == 1:
                yield Text('1 kus materiálu'.format(count))
            elif 2 <= count <= 4:
                yield Text('{} kusy materiálu'.format(count))
            else:
                yield Text('{} kusov materiálu'.format(count))

            yield self._activity_link(activity.target_link)
        elif activity.action_type in (ActivityType.BORROW_MATERIAL_ITEMS, ActivityType.RETURN_MATERIAL_ITEMS):
            action_token = action_lookup({
                ActivityType.BORROW_MATERIAL_ITEMS: ('si požičal', 'si požičala'),
                ActivityType.RETURN_MATERIAL_ITEMS: ('vrátil', 'vrátila'),
            })

            yield actor_token
            yield action_token

            count = sum(material['count'] for material in activity.details['material_summary'])
            if count == 1:
                yield Text('1 kus materiálu'.format(count))
            elif 2 <= count <= 4:
                yield Text('{} kusy materiálu'.format(count))
            else:
                yield Text('{} kusov materiálu'.format(count))
        else:
            raise ValueError('Unexpected activity type {!r}'.format(activity.action_type))

    def html(self, activity):
        return format_html_join(' ', '{}', ((token.as_html(),) for token in self.tokens(activity)))

    def text(self, activity):
        return ' '.join(token.as_text() for token in self.tokens(activity))
