from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from horam.web.models import Discussion, User, Person, Event, Comment


class PersonInline(admin.StackedInline):
    model = Person


class ExtendedUserAdmin(UserAdmin):
    inlines = UserAdmin.inlines + [PersonInline]
    list_display = ('username', 'email', 'is_staff', 'last_login')


admin.site.register(User, ExtendedUserAdmin)
admin.site.register(Discussion)
admin.site.register(Comment)
admin.site.register(Person)
admin.site.register(Event)

