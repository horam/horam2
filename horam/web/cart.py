from typing import Dict, Any

from horam.web.forms import MaterialCartFormset
from horam.web.models import MaterialModel


class MaterialCartItem:
    def __init__(self, model, count):
        self.model = model
        self.count = count


class MaterialCart:
    SESSION_KEY = 'material_cart'

    def __init__(self, session):
        self.session = session

        cart_data = session.get(self.SESSION_KEY, {})
        self.content_by_model = {}
        if cart_data:
            items = cart_data['items']
            model_by_id = {}
            for model in MaterialModel.objects.filter(id__in=[item['model_id'] for item in items]):
                model_by_id[model.pk] = model
            for item in items:
                self.content_by_model[item['model_id']] = MaterialCartItem(model_by_id[item['model_id']], item['count'])

    def json(self) -> Dict[str, Any]:
        """Return the cart state as a dictionary serializable as json"""
        return {
            'items': [{
                'model_id': item.model.id,
                'count': item.count,
            } for item in self.content_by_model.values()]
        }

    def add_model(self, model: MaterialModel, count: int):
        """Add a given model to cart count times

        :param model: The MaterialModel to add
        :param count: How many times to add to the cart

        """
        if model.pk in self.content_by_model:
            content = self.content_by_model[model.pk]
        else:
            content = MaterialCartItem(model, 0)
            self.content_by_model[model.pk] = content
        content.count += count

    def contents(self):
        return self.content_by_model.values()

    def reset(self):
        """Reset the cart to empty state"""
        self.content_by_model = {}

    def save(self):
        self.session[self.SESSION_KEY] = self.json()

    def as_formset(self, *args, **kwargs):
        kwargs['initial'] = [{
            'model': item.model,
            'count': item.count,
        } for item in self.contents()]
        return MaterialCartFormset(*args, **kwargs)

    def update_from_formset(self, formset):
        self.reset()
        for form in formset:
            if form.is_valid() and 'model' in form.cleaned_data and 'count' in form.cleaned_data:
                self.add_model(form.cleaned_data['model'], form.cleaned_data['count'])
