import datetime
from typing import Optional

from django.conf import settings
from django.contrib.auth import get_user_model
from django.core.exceptions import ValidationError
from django.forms import ModelForm, Form, HiddenInput, IntegerField, CharField, Textarea, BooleanField, \
    inlineformset_factory, DateField, ModelChoiceField, formset_factory, ChoiceField, Field
from django.template.defaultfilters import filesizeformat
from enumfields import EnumField
from psycopg2.extras import DateRange

from horam.web.models import Event, Review, Topic, Person, Photo, ExtendedParticipationState, \
    NotificationSendLevel, User, BlogArticle, MaterialModel, MaterialItem, MaterialLoan, has_bounds


class EventForm(ModelForm):
    class Meta:
        model = Event
        fields = ['name', 'state', 'date_start', 'duration_days', 'latitude', 'longitude', 'organizers', 'description']

    notification_level = EnumField(NotificationSendLevel, verbose_name='Notifikácie',
                                   default=NotificationSendLevel.DEFAULT).formfield()


class ReviewForm(ModelForm):
    class Meta:
        model = Review
        fields = ['authors', 'content']


class CommentForm(Form):
    parent = IntegerField(widget=HiddenInput(), required=False)
    content = CharField(widget=Textarea(), label='Komentár')


class TopicForm(ModelForm):
    class Meta:
        model = Topic
        fields = ['title', 'description']


class ProfileForm(Form):
    username = get_user_model()._meta.get_field('username').formfield()
    sex = Person._meta.get_field('sex').formfield()
    email = get_user_model()._meta.get_field('email').formfield()
    avatar = Person._meta.get_field('avatar').formfield()
    new_event_notifications = User._meta.get_field('new_event_notifications').formfield()
    event_notification_level = User._meta.get_field('event_notification_level').formfield()
    topic_notifications = User._meta.get_field('topic_notifications').formfield()

    def clean_avatar(self):
        content = self.cleaned_data['avatar']
        if content and content.size > settings.MAX_IMAGE_SIZE_BYTES:
            raise ValidationError('Veľkosť obrázka ({}) je viac ako limit {}, prosím použi menší obrázok'
                                  .format(filesizeformat(content.size), filesizeformat(settings.MAX_IMAGE_SIZE_BYTES)))

        return content


class DeactivateForm(Form):
    confirmation = BooleanField(label='Naozaj deaktivovať')


class PhotoForm(ModelForm):
    class Meta:
        model = Photo
        fields = ['image']

    def clean_image(self):
        content = self.cleaned_data['image']
        if content.size > settings.MAX_IMAGE_SIZE_BYTES:
            raise ValidationError('Veľkosť obrázka ({}) je viac ako limit {}, prosím použi menší obrázok'
                                  .format(filesizeformat(content.size), filesizeformat(settings.MAX_IMAGE_SIZE_BYTES)))

        return content


class ParticipationForm(Form):
    participation = EnumField(ExtendedParticipationState, verbose_name='Moja účasť',
                              default=ExtendedParticipationState.UNKNOWN).formfield()


class BlogArticleForm(ModelForm):
    class Meta:
        model = BlogArticle
        fields = ['title', 'content']


class MaterialModelForm(ModelForm):
    class Meta:
        model = MaterialModel
        fields = ['name', 'category', 'price_eur', 'description']


class MaterialCommissionForm(Form):
    commission_count = IntegerField(min_value=0, label='Počet')
    commission_date = DateField(label='Dátum zaradenia', required=False)


class MaterialDecommissionForm(Form):
    decommission_date = DateField(label='Dátum vyradenia', required=False)


class MaterialCartItemForm(Form):
    model = ModelChoiceField(MaterialModel.objects.filter(can_borrow=True), label='Model materiálu')
    count = IntegerField(min_value=1, label='Počet')


MaterialCartFormset = formset_factory(MaterialCartItemForm)


class MyDateRangeField(Field):
    date_format = '%d.%m.%Y'

    def to_python(self, value) -> Optional[DateRange]:
        if value in self.empty_values:
            return None
        if isinstance(value, DateRange):
            return value
        parts = [x.strip() for x in value.split('-')]
        if len(parts) > 2:
            raise ValidationError('Rozsah dátumov nemôže mať viac ako dve časti')
        parsed_parts = []
        for part in parts:
            try:
                parsed = datetime.datetime.strptime(part, self.date_format).date()
            except ValueError:
                raise ValidationError('Nesprávny dátum {!r} - dátum musí mať formát deň.mesiac.rok'.format(part))
            parsed_parts.append(parsed)
        if len(parsed_parts) == 1:
            return DateRange(parsed_parts[0], parsed_parts[0], bounds='[]')
        if parsed_parts[1] < parsed_parts[0]:
            raise ValidationError('Dátum konca nemôže byť skôr ako dátum začiatku')
        return DateRange(parsed_parts[0], parsed_parts[1], bounds='[]')

    def prepare_value(self, value):
        try:
            value = self.clean(value)
        except ValidationError:
            # Return invalid values as-is
            return value

        if not isinstance(value, DateRange):
            raise TypeError('prepare_value needs DateRange')

        lower, upper = value.lower, value.upper
        if not value.upper_inc:
            upper = upper - datetime.timedelta(1)

        if lower == upper:
            parts = (lower,)
        else:
            parts = (lower, upper)

        return '-'.join(x.strftime(self.date_format) for x in parts)


class MaterialCartLoanForm(Form):
    during = MyDateRangeField(validators=[has_bounds], label=MaterialLoan._meta.get_field('during').verbose_name,
                              required=True, label_suffix=' ( dátum vo formáte d.m.r alebo rozsah d.m.r-d.m.r )')


class MaterialReturnItemForm(Form):
    model = ModelChoiceField(MaterialModel.objects.all(), label='Model materiálu', required=True, widget=HiddenInput())
    during = MyDateRangeField(validators=[has_bounds], label=MaterialLoan._meta.get_field('during').verbose_name,
                              required=True, widget=HiddenInput())
    count = IntegerField(min_value=0, label='Počet', required=True)


MaterialReturnFormset = formset_factory(MaterialReturnItemForm, extra=0)
