import difflib
from collections import defaultdict
from urllib.parse import urlparse, urlunparse

import requests
from django.core.management.base import BaseCommand
from django.db import transaction

from horam.web.models import all_user_content
from horam.web.user_content import load_raw_content, ContentFilter, dump_raw_content


class ExtractLinks(ContentFilter):
    def __init__(self):
        self.links = {}

    def link(self, href):
        if href not in self.links:
            self.links[href] = False
        return href

    def image(self, src):
        self.links[src] = True
        return src


class ReplaceLinks(ContentFilter):
    def __init__(self, replacements):
        self._replacements = replacements

    def link(self, href):
        return self._replacements.get(href, href)

    def image(self, src):
        return self._replacements.get(src, src)


class URLReport:
    def __init__(self, url, message, *, replace_url=None):
        self.url = url
        self.message = message
        self.replace_url = replace_url


class LinkChecker:
    def __init__(self, allow_remote=False, out=None):
        self.allow_remote = allow_remote
        self.reports = []
        self.out = out

    def _report(self, message):
        self.reports.append(message)

    def _session(self):
        session = requests.session()
        session.headers.update({'User-Agent': 'HoramLinkCheckBot/1.0 (+https://horam.sk)'})
        return session

    def _get(self, url, **kwargs):
        if self.out:
            self.out.write('GET {}'.format(url))
        return self._session().get(url, **kwargs)

    def _check_remote_url(self, url, parsed_url, report_http=False):
        if parsed_url.scheme not in ('http', 'https'):
            return None

        try:
            response = self._session().get(url, stream=True)
        except requests.exceptions.RequestException as e:
            return URLReport(url, 'could not download: {}'.format(e))
        with response:
            response_url_parsed = urlparse(response.url)
            if response.status_code in [404, 410]:
                return URLReport(url, 'broken link')
            elif response.status_code >= 300:
                return URLReport(url, 'error retrieving URL: HTTP {}'.format(response.status_code))
            elif response.history and response_url_parsed.scheme == 'https':  # request was redirected to HTTPS
                return URLReport(url, 'redirects to https location', replace_url=response.url)
            elif parsed_url.scheme == 'http':
                # try if the same content is present using HTTPS
                https_url = urlunparse(('https',) + parsed_url[1:])
                try:
                    response2 = self._session().get(https_url)
                except requests.exceptions.RequestException as e:
                    if report_http:
                        return URLReport(url, 'cannot upgrade to https: {}'.format(e))
                    else:
                        return None
                else:
                    if 200 >= response.status_code < 300 and response.content == response2.content:
                        return URLReport(url, 'upgrade to https possible', replace_url=https_url)
                    elif report_http:
                        return URLReport(url, 'cannot upgrade to https')
                    else:
                        return None
            elif response.history:
                return URLReport(url, 'redirects to another location, cannot upgrade to https'.format(url),
                                 replace_url=response.url)

        return None

    def check_url(self, url, report_http=False):
        parsed_url = urlparse(url.replace('http:///', 'http://'))

        if parsed_url.scheme in ('http', 'https') and not parsed_url.hostname:
            return URLReport(url, 'invalid_url')

        if parsed_url.hostname == 'horam.sk':
            fixed = urlunparse(('', '') + parsed_url[2:])
            if fixed:
                return URLReport(url, 'absolute url to our content', replace_url=fixed)
            else:
                return URLReport(url, 'absolute url to our content')
        elif self.allow_remote:
            return self._check_remote_url(url, parsed_url, report_http=report_http)

        return None


class SectionWriter:
    def __init__(self, out):
        self._out = out
        self._header = None
        self._written_header = False

    def start(self, title):
        self._header = title
        self._written_header = False

    def write(self, line, **kwargs):
        if not self._written_header:
            self._out.write(self._header)
            self._written_header = True
        self._out.write(line, **kwargs)


class Command(BaseCommand):
    help = 'Checks user content for broken links, etc.'

    def add_arguments(self, parser):
        parser.add_argument('--allow-remote', action='store_true')
        parser.add_argument('--diff', action='store_true')

    def handle(self, *args, **options):
        writer = SectionWriter(self.stdout)

        with transaction.atomic():
            for model_cls, model_instance, field_name in all_user_content():
                raw_content = getattr(model_instance, field_name)
                content = load_raw_content(raw_content)
                where = '{}:{}'.format(model_cls._meta.db_table, model_instance.pk)
                extract_links = ExtractLinks()
                content.filter(extract_links)

                try:
                    writer.start('------- {} ({})'.format(where, model_instance.get_absolute_url()))
                except AttributeError:
                    writer.start('------- {}'.format(where))

                checker = LinkChecker(allow_remote=options['allow_remote'], out=writer)
                replacements = {}
                for url, report_http in extract_links.links.items():
                    report = checker.check_url(url, report_http=report_http)
                    if not report:
                        continue
                    if report.replace_url:
                        replacements[report.url] = report.replace_url
                    line = '* {} {}'.format(report.message, report.url)
                    if report.replace_url:
                        line += ' → {}'.format(report.replace_url)
                    writer.write(line)

                filtered_content = content.filter(ReplaceLinks(replacements))
                filtered_raw = dump_raw_content(filtered_content)

                if filtered_raw != raw_content:
                    if options['diff']:
                        diff = '\n'.join(difflib.unified_diff(raw_content.splitlines(), filtered_raw.splitlines(),
                                                              lineterm=''))
                        writer.write(diff)
