import difflib

from django.core.management.base import BaseCommand, CommandError

from horam.web.models import all_user_content
from horam.web.user_content import load_raw_content, ContentFilter


class Command(BaseCommand):
    help = 'Checks that our filter implementation does not break user content present in database'

    def expect_equal(self, a, b, msg):
        if a == b:
            return True

        diff = '\n'.join(difflib.unified_diff(a.splitlines(), b.splitlines(), lineterm=''))

        self.stdout.write(msg)
        self.stdout.write(diff)
        self.stdout.write('{!r} != {!r}'.format(a, b))
        return False

    def _process_content(self, object_type, object_id, raw_content):
        content = load_raw_content(raw_content)
        filtered_content = content.filter(ContentFilter())
        return self.expect_equal(content.raw_content, filtered_content.raw_content,
                                 '{}:{} differs!'.format(object_type, object_id))

    def handle(self, *args, **options):
        success = True
        for model_cls, model_instance, field_name in all_user_content():
            raw_content = getattr(model_instance, field_name)
            content = load_raw_content(raw_content)
            msg = '{}:{} differs!'.format(model_cls._meta.db_table, model_instance.pk)
            filtered_content = content.filter(ContentFilter())
            success = success and self.expect_equal(content.raw_content, filtered_content.raw_content, msg)

        if not success:
            raise CommandError('Some changes detected')
