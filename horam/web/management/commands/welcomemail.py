from django.contrib.auth import get_user_model
from django.core import mail
from django.core.management.base import BaseCommand
from django.template.loader import render_to_string
from django.urls import reverse


class Command(BaseCommand):
    help = 'Send welcome mail'

    def add_arguments(self, parser):
        parser.add_argument('username')

    def handle(self, *args, **options):
        user = get_user_model().objects.get(username=options['username'])

        base_url = 'https://horam.sk'

        email_context = {
            'user': user,
            'password_reset_link': base_url + reverse('password_reset'),
            'profile_edit_link': base_url + reverse('members_edit', kwargs={'username': user.username})
        }

        with mail.get_connection() as connection:
            text_body = render_to_string('emails/welcome.txt', context=email_context)
            html_body = render_to_string('emails/welcome.html', context=email_context)
            message = mail.EmailMultiAlternatives(
                subject='[horam] Vitaj na horam.sk!',
                body=text_body,
                from_email=None,
                to=[user.email],
                connection=connection
            )
            message.attach_alternative(html_body, 'text/html')
            message.send()
