import ipaddress

import user_sessions.middleware

from django.conf import settings

try:
    from importlib import import_module
except ImportError:
    from django.utils.importlib import import_module


def _get_forwarded_for(request):
    ips = request.META.get('HTTP_X_FORWARDED_FOR', '')
    return [ipaddress.ip_address(x.strip()) for x in ips.split(',')]


def _get_remote_addr(request):
    remote_addr = request.META.get('REMOTE_ADDR', '')
    if remote_addr == '':
        return None
    return ipaddress.ip_address(remote_addr)


def _get_client_ip(request):
    try:
        ips = _get_forwarded_for(request)
    except ValueError:
        ips = []

    remote_addr = _get_remote_addr(request)
    # remote_addr might be None if the remote IP is not available, i.e. when proxy is connected using unix socket
    if remote_addr is not None:
        ips.append(ipaddress.ip_address(remote_addr))

    trusted_ips = [ipaddress.ip_network(x) for x in settings.TRUSTED_PROXIES]

    for ip in reversed(ips):
        if not any(ip in trusted_ip for trusted_ip in trusted_ips):
            return ip

    return None


class SessionMiddleware(user_sessions.middleware.SessionMiddleware):
    def process_request(self, request):
        engine = import_module(settings.SESSION_ENGINE)
        session_key = request.COOKIES.get(settings.SESSION_COOKIE_NAME, None)
        remote_ip = _get_client_ip(request)
        request.session = engine.SessionStore(
            ip=str(remote_ip) if remote_ip is not None else '',
            user_agent=request.META.get('HTTP_USER_AGENT', ''),
            session_key=session_key
        )
