# Generated by Django 2.1 on 2018-08-24 11:29

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('web', '0010_auto_20180824_1107'),
    ]

    operations = [
        migrations.AddField(
            model_name='activitylink',
            name='comment',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='web.Comment'),
        ),
        migrations.AddField(
            model_name='activitylink',
            name='photo',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='web.Photo'),
        ),
    ]
