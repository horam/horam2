# Generated by Django 2.1.5 on 2019-03-03 08:15

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('web', '0028_materialcategory_order'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='materialmodel',
            name='image',
        ),
    ]
