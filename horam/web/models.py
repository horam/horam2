import collections
import datetime
import itertools
import psycopg2.extras
import secrets
import os.path
import hashlib
from typing import Iterable, Tuple, Dict

import unicodedata

from django.contrib.postgres.fields import DateRangeField
from django.core.exceptions import ObjectDoesNotExist, ValidationError
from django.db import models, transaction
from django.conf import settings
from django.db.models import BooleanField, JSONField
from django.forms import Textarea
from django.urls import reverse
from django.utils import timezone
from django.contrib.auth.models import AbstractUser, UserManager
from enumfields import EnumField, Enum, EnumIntegerField
from imagekit.models import ImageSpecField
from imagekit.processors import SmartResize
from psycopg2._range import DateRange
from randomcolor import RandomColor


class Sex(Enum):
    MALE = 'm'
    FEMALE = 'f'

    class Labels:
        MALE = 'Muž'
        FEMALE = 'Žena'


class EventState(Enum):
    TENTATIVE = 't'
    CONFIRMED = 'c'
    CANCELLED = 'x'
    DONE = 'd'

    class Labels:
        TENTATIVE = 'Plánovaná'
        CONFIRMED = 'Potvrdená'
        CANCELLED = 'Zrušená'
        DONE = 'Uskutočnená'


class ParticipationState(Enum):
    PARTICIPANT = 'p'  # Wants to participate in the event
    NONPARTICIPANT = 'n'  # Does not want to participate in the event

    class Labels:
        PARTICIPANT = 'zúčastní sa'
        NONPARTICIPANT = 'nezúčastní sa'


class ExtendedParticipationState(Enum):
    PARTICIPANT = 'p'  # Wants to participate in the event
    NONPARTICIPANT = 'n'  # Does not want to participate in the event
    UNKNOWN = '?'  # Unknown participation state

    class Labels:
        PARTICIPANT = 'zúčastním sa'
        NONPARTICIPANT = 'nezúčastním sa'
        UNKNOWN = 'neviem'


class ActivityType(Enum):
    ADD_COMMENT = 'add_comment'
    UPDATE_COMMENT = 'update_comment'
    DELETE_COMMENT = 'delete_comment'

    UPDATE_PAGE = 'update_page'

    ADD_EVENT = 'add_event'
    UPDATE_EVENT = 'update_event'
    DELETE_EVENT = 'delete_event'
    UPDATE_PARTICIPATION_STATE = 'update_participation_state'

    ADD_REVIEW = 'add_review'
    UPDATE_REVIEW = 'update_review'

    ADD_TOPIC = 'add_topic'
    UPDATE_TOPIC = 'update_topic'
    DELETE_TOPIC = 'delete_topic'

    ADD_USER = 'add_user'
    DELETE_USER = 'delete_user'
    ACTIVATE_USER = 'activate_user'
    DEACTIVATE_USER = 'deactivate_user'

    ADD_PHOTO = 'add_photo'
    UPDATE_PHOTO = 'update_photo'

    ADD_BLOG_ARTICLE = 'add_blog_article'
    UPDATE_BLOG_ARTICLE = 'update_blog_article'

    ADD_MATERIAL_MODEL = 'add_material_model'
    UPDATE_MATERIAL_MODEL = 'update_material_model'
    COMMISSION_MATERIAL_ITEM = 'commission_material_item'
    DECOMMISSION_MATERIAL_ITEM = 'decommission_material_item'
    BORROW_MATERIAL_ITEMS = 'borrow_material_items'
    RETURN_MATERIAL_ITEMS = 'return_material_items'


class ActivityLinkType(Enum):
    EVENT = 'event'
    TOPIC = 'topic'
    PAGE = 'page'
    PERSON = 'person'
    COMMENT = 'comment'
    PHOTO = 'photo'
    BLOG_ARTICLE = 'blog_article'
    MATERIAL_MODEL = 'material_model'
    MATERIAL_ITEMS = 'material_items'


class EventNotificationLevel(Enum):
    NEVER = 0  # Never send event e-mail
    OPT_IN = 1  # Only send when participating in an event
    OPT_OUT = 2  # Send if not opted out
    ALWAYS = 3  # Always send event e-mail

    class Labels:
        NEVER = 'nikdy'
        OPT_IN = 'iba kde mám "zúčastním sa"'
        OPT_OUT = 'iba kde nemám "nezúčastním sa"'
        ALWAYS = 'vždy'


class NotificationSendLevel(Enum):
    SUPPRESS = 3
    DEFAULT = 2
    FORCE = 1

    class Labels:
        SUPPRESS = 'neposielať'
        DEFAULT = 'podľa nastavení'
        FORCE = 'poslať všetkým'


# Fix enums so they work in templates, see https://stackoverflow.com/a/35953630
for enum_class in (Sex, EventState, ParticipationState, ExtendedParticipationState, ActivityType,
                   EventNotificationLevel, NotificationSendLevel):
    enum_class.do_not_call_in_templates = True


class HoramUserManager(UserManager):
    def create_superuser(self, username, email, password, **extra_fields):
        user = super().create_superuser(username, email, password, **extra_fields)
        # Create a person for superuser
        profile = Person(
            user=user,
            nickname=username,
            sex=Sex.MALE,
        )
        profile.save()
        return user


class User(AbstractUser):
    class Meta:
        # We use user_account as user is a reserved word
        # django handles it well, but 'select * from user' returns different things than 'select * from "user"'
        # so we change the name to avoid confusion when using the DB manually.
        db_table = 'user_account'

    objects = HoramUserManager()

    event_notification_level = EnumIntegerField(EventNotificationLevel,
                                                verbose_name='Notifikácie o zmenách/komentároch v akciách',
                                                default=EventNotificationLevel.OPT_IN)
    new_event_notifications = BooleanField(default=True, verbose_name='Notifikácie o nových akciách')
    topic_notifications = BooleanField(default=True, verbose_name='Notifikácie o zmenách/komentároch vo fóre')

    def get_absolute_url(self):
        return reverse('members_detail', args=(self.username,))


def avatar_upload_path(person, filename):
    _, ext = os.path.splitext(filename)
    ext = ext.lower()
    if ext not in ('.jpg', '.jpeg', '.png'):
        ext = '.bin'
    return 'avatars/{}/{}{}'.format(person.id, secrets.token_hex(), ext)


def extract_avatar_initials(s):
    initials = []
    prev = None
    prev_category = None
    secondary = None
    capture_secondary = False
    for c in s:
        category = unicodedata.category(c)
        if category[0] in 'PZ':
            # skip punctuation or separator
            pass
        else:
            if secondary is None and capture_secondary:
                secondary = c
            is_initial = (
                # first non-space/separator character
                    prev is None or
                    # Lowercase letter followed by uppercase or titlecase letter
                    (prev_category == 'Ll' and category in ('Lu', 'Lt')) or
                    # number after letter
                    (prev_category[0] == 'L' and category[0] == 'N') or
                    # letter or number after punctuation or separator
                    (prev_category[0] in 'PZ' and category[0] in 'LN')
            )
            if is_initial:
                initials.append(c)
                capture_secondary = True
        prev = c
        prev_category = category

    if len(initials) == 1:
        initials.append(secondary)

    return ''.join(initials)


class Person(models.Model):
    class Meta:
        db_table = 'person'

    user = models.OneToOneField(settings.AUTH_USER_MODEL, blank=True, null=True, on_delete=models.PROTECT)
    nickname = models.CharField(
        max_length=150,
        unique=True,
        help_text='Required. 150 characters or fewer. Letters, digits and @/./+/-/_ only.',
        validators=[User.username_validator],
        error_messages={
            'unique': "A person with that nickname already exists.",
        },
        verbose_name='Prezývka',
    )
    sex = EnumField(Sex, max_length=1, verbose_name='Pohlavie')
    avatar = models.ImageField(upload_to=avatar_upload_path, blank=True, null=True)
    avatar_thumbnail = ImageSpecField(source='avatar',
                                      processors=[SmartResize(64, 64)],
                                      format='JPEG',
                                      options={'quality': 90})

    def __str__(self):
        return self.nickname

    @property
    def is_male(self):
        return self.sex == Sex.MALE

    @property
    def is_female(self):
        return self.sex == Sex.FEMALE

    def default_avatar_initials(self):
        return extract_avatar_initials(self.nickname)

    def default_avatar_color(self):
        return RandomColor(seed=hashlib.md5(self.nickname.encode('utf8')).digest()).generate(luminosity='dark')[0]

    def _get_absolute_url(self):
        # Only implemented for persons which have corresponding user, so it's hidden using __getattr__ for those
        # that don't have a URL
        return self.user.get_absolute_url()

    def __getattr__(self, item):
        if item == 'get_absolute_url':
            if self.user is None or not self.user.is_active:
                raise AttributeError('get_absolute_url not available for Persons without a User')
            else:
                return self._get_absolute_url
        return super(Person, self).__getattr__(self, item)


class Discussion(models.Model):
    class Meta:
        db_table = 'discussion'


class CommentManager(models.Manager):
    CommentTreeResult = collections.namedtuple('CommentTreeResult',
                                               'id parent_id author_id created modified content path '
                                               'author_user_id author_nickname author_sex author_avatar')

    def discussion_tree(self, discussion_id, per_comment=None):
        """Recursively fetch discussion comments for discussion.

        :param discussion_id: Integer id of the discussion to fetch comments for
        :param per_comment: a function fn(comment) to perform additional modifications to the returned objects.
                            It should perform in-place modification to the object. The function may be omitted.

        :return: A list of Comment objects with some additional attributes:
                 - children: a list of children Comment objects
                 - path: a list of comments ids from root to this comment
                 - max_created: max of this comment's and all childrens' (recursively) created date
        """
        from django.db import connection

        query = '''
            WITH RECURSIVE tree
            AS
            (
                SELECT
                    id, parent_id, author_id, created, modified, content, ARRAY[id] AS path
                FROM comment
                WHERE parent_id IS NULL AND discussion_id = %s
                UNION
                SELECT
                    c2.id, c2.parent_id, c2.author_id, c2.created, c2.modified, c2.content, tree.path || c2.id AS path
                FROM
                    tree
                    JOIN comment c2 ON c2.parent_id = tree.id
            )
            SELECT tree.id, tree.parent_id, tree.author_id, tree.created, tree.modified, tree.content, tree.path,
                   person.user_id as author_user_id, person.nickname as author_nickname, person.sex as author_sex,
                   person.avatar as author_avatar
            FROM tree
            LEFT JOIN person ON tree.author_id = person.id
            ORDER BY path
            '''

        with connection.cursor() as cursor:
            cursor.execute(query, (discussion_id,))
            root = []
            stack = []

            def key(x):
                return x if x is not None else datetime.datetime.utcfromtimestamp(0).replace(tzinfo=datetime.timezone.utc)

            for row in cursor:
                row = self.CommentTreeResult(*row)  # add names to indexes
                depth = len(row.path) - 1
                for leaving_child in reversed(stack[depth:]):
                    leaving_child.children.sort(key=lambda x: key(x.max_created))
                    leaving_child.max_created = max(itertools.chain([leaving_child.max_created],
                                                                    (x.max_created for x in leaving_child.children)),
                                                    key=key)
                stack = stack[:depth]
                author = Person(
                    id=row.author_id,
                    user_id=row.author_user_id,
                    nickname=row.author_nickname,
                    sex=row.author_sex,
                    avatar=row.author_avatar
                )
                comment = Comment(
                    id=row.id,
                    parent_id=row.parent_id,
                    author=author,
                    created=row.created,
                    modified=row.modified,
                    content=row.content,
                )
                # Add extra attributes not defined in the model
                comment.max_created = row.created
                comment.path = row.path
                comment.children = []
                if per_comment:
                     per_comment(comment)
                if len(stack) == 0:
                    root.append(comment)
                else:
                    assert stack[-1].id == comment.parent_id
                    stack[-1].children.append(comment)
                stack.append(comment)

        root.sort(key=lambda x: key(x.max_created), reverse=True)
        return root


class FormattedTextField(models.TextField):

    def formfield(self, **kwargs):
        defaults = {}
        if not self.choices:
            defaults['widget'] = Textarea(attrs={'class': 'formatted-content'})
        defaults.update(kwargs)
        return super().formfield(**defaults)


class Comment(models.Model):
    class Meta:
        db_table = 'comment'

    parent = models.ForeignKey('self', on_delete=models.PROTECT, null=True, blank=True)
    discussion = models.ForeignKey(Discussion, on_delete=models.PROTECT, related_name='comments')
    author = models.ForeignKey(Person, on_delete=models.PROTECT)
    created = models.DateTimeField(default=timezone.now)
    modified = models.DateTimeField(blank=True, null=True)
    content = FormattedTextField()

    objects = CommentManager()


class Event(models.Model):
    class Meta:
        db_table = 'event'

    state = EnumField(EventState, max_length=1, verbose_name='Stav')
    name = models.CharField(max_length=100, verbose_name='Názov')
    date_start = models.DateField(null=True, verbose_name='Začiatok', blank=True)
    # TODO in django 2.2, add check (duration_days is null or duration_days > 0),
    duration_days = models.PositiveIntegerField(null=True, blank=True, verbose_name='Počet dní')
    description = FormattedTextField(verbose_name='Popis')
    discussion = models.OneToOneField(Discussion, on_delete=models.PROTECT)
    organizers = models.ManyToManyField(Person, related_name='organizes', blank=True, verbose_name='Organizátori')
    participants = models.ManyToManyField(Person, through='Participation',
                                          related_name='participates_in', blank=True, verbose_name='Účasť')
    slug = models.SlugField()
    latitude = models.DecimalField(max_digits=10, decimal_places=7, null=True, blank=True,
                                   verbose_name='Zemepisná šírka (° severne)')
    longitude = models.DecimalField(max_digits=10, decimal_places=7, null=True, blank=True,
                                    verbose_name='Zemepisná dĺžka (° východne)')

    @property
    def date_end(self):
        if not (self.date_start and self.duration_days):
            return None

        return self.date_start + datetime.timedelta(days=(self.duration_days - 1))

    @property
    def date_end_exclusive(self):
        if not (self.date_start and self.duration_days):
            return None

        return self.date_start + datetime.timedelta(days=self.duration_days)

    def get_absolute_url(self):
        return reverse('events_detail', args=(self.id, self.slug))

    @property
    def is_cancelled(self):
        return self.state == EventState.CANCELLED

    @property
    def is_done(self):
        return self.state == EventState.DONE

    @property
    def is_tentative(self):
        return self.state == EventState.TENTATIVE

    @property
    def is_confirmed(self):
        return self.state == EventState.CONFIRMED

    @property
    def should_have_review(self):
        return self.is_cancelled or self.is_done

    @property
    def is_planned(self):
        return self.is_tentative or self.is_confirmed


class Participation(models.Model):
    class Meta:
        db_table = 'event_participation'

    event = models.ForeignKey(Event, on_delete=models.CASCADE)
    person = models.ForeignKey(Person, on_delete=models.CASCADE)
    state = EnumField(ParticipationState, max_length=1)


def get_extended_participation_state(event, person):
    try:
        participation = Participation.objects.get(event=event, person=person)
    except Participation.DoesNotExist:
        return ExtendedParticipationState.UNKNOWN
    else:
        return ExtendedParticipationState(participation.state.value)


def set_extended_participation_state(event, person, extended_state):
    if extended_state == ExtendedParticipationState.UNKNOWN:
        Participation.objects.filter(event=event, person=person).delete()
    else:
        Participation.objects.update_or_create(event=event, person=person, defaults={
            'state': ParticipationState(extended_state.value)
        })


class Review(models.Model):
    class Meta:
        db_table = 'event_review'

    event = models.OneToOneField(Event, on_delete=models.PROTECT, primary_key=True, verbose_name='Akcia')
    authors = models.ManyToManyField(Person, related_name='reviews', blank=True, verbose_name='Autori')
    content = FormattedTextField(verbose_name='Záznam')


def photo_upload_path(photo, filename):
    _, ext = os.path.splitext(filename)
    ext = ext.lower()
    if ext not in ('.jpg', '.jpeg', '.png'):
        ext = '.bin'
    return 'akcie/{}/{}{}'.format(photo.event_id, secrets.token_hex(), ext)


class Photo(models.Model):
    class Meta:
        db_table = 'photo'

    event = models.ForeignKey(Event, on_delete=models.PROTECT, verbose_name='Akcia', related_name='photos')
    image = models.ImageField(upload_to=photo_upload_path)
    uploaded_by = models.ForeignKey(User, on_delete=models.PROTECT, verbose_name='Uploadol', null=True)


class Topic(models.Model):
    class Meta:
        db_table = 'topic'

    author = models.ForeignKey(Person, on_delete=models.PROTECT, verbose_name='Autor')
    created = models.DateTimeField(default=timezone.now, verbose_name='Vytvorené')
    title = models.CharField(max_length=100, verbose_name='Názov')
    description = FormattedTextField(verbose_name='Popis')
    discussion = models.OneToOneField(Discussion, on_delete=models.PROTECT)
    slug = models.SlugField()

    def get_absolute_url(self):
        return reverse('topics_detail', args=(self.id, self.slug))


class ActivityLink(models.Model):
    """A model that links activity object or target.

    At most one of the foreign keys should be set.

    We use this instead of the django contenttypes framework as we want to have referential integrity
    in the database. See e.g. https://lukeplant.me.uk/blog/posts/avoid-django-genericforeignkey/ for
    arguments against GenericForeignKey.
    """
    class Meta:
        db_table = 'activity_link'

    # ──────────────────────────────────────────────────────────────────────────────────────────────
    # Relations to models
    # TODO: enforce at most one link in django 2.2 using check constraints
    event = models.ForeignKey(Event, on_delete=models.PROTECT, null=True, blank=True)
    topic = models.ForeignKey(Topic, on_delete=models.PROTECT, null=True, blank=True)
    person = models.ForeignKey(Person, on_delete=models.PROTECT, null=True, blank=True)
    comment = models.ForeignKey(Comment, on_delete=models.PROTECT, null=True, blank=True)
    photo = models.ForeignKey(Photo, on_delete=models.PROTECT, null=True, blank=True)
    blog_article = models.ForeignKey('BlogArticle', on_delete=models.PROTECT, null=True, blank=True)
    material_model = models.ForeignKey('MaterialModel', on_delete=models.PROTECT, null=True, blank=True)
    material_items = models.ManyToManyField('MaterialItem', db_table='activity_link_material_item')

    # ──────────────────────────────────────────────────────────────────────────────────────────────
    # Additional information about the link (useful mainly due to the fact that some information is
    # missing from the historically imported data from the old site.

    # What kind of relation is this? Roughly corresponds to the non-empty relation above.
    linked_type = EnumField(ActivityLinkType, max_length=32)

    # Display name of the linked object at the time of the activity (i.e. event name, topic name, etc.)
    # Used for displaying activities even for deleted objects.
    linked_name = models.CharField(max_length=255)

    def _get_linked_field(self):
        for field in self._meta.get_fields():
            if field.is_relation and field.many_to_one:
                linked_id = getattr(self, field.get_attname())
                if linked_id is not None:
                    return field, linked_id
        raise ObjectDoesNotExist('ActivityLink does not have any relation set')

    @property
    def linked_object(self):
        """linked_object is the model instance referenced by this ActivityLink.

        For example this might be an instance of Event.

        :raise ObjectDoesNotExist if there is no linked object
        """

        field, linked_id = self._get_linked_field()
        return getattr(self, field.name)

    @property
    def linked_object_id(self):
        """linked_object_id is the id of the model instance referenced by this ActivityLink.

        :raise ObjectDoesNotExist if there is no linked object
        """

        field, linked_id = self._get_linked_field()
        return linked_id

    @classmethod
    def for_event(cls, event):
        return cls(
            event=event,
            linked_type=ActivityLinkType.EVENT,
            linked_name=event.name,
        )

    @classmethod
    def for_topic(cls, topic):
        return cls(
            topic=topic,
            linked_type=ActivityLinkType.TOPIC,
            linked_name=topic.title,
        )

    @classmethod
    def for_person(cls, person):
        return cls(
            person=person,
            linked_type=ActivityLinkType.PERSON,
            linked_name=person.nickname,
        )

    @classmethod
    def for_comment(cls, comment):
        return cls(
            comment=comment,
            linked_type=ActivityLinkType.COMMENT,
            linked_name='',
        )

    @classmethod
    def for_photo(cls, photo):
        return cls(
            photo=photo,
            linked_type=ActivityLinkType.PHOTO,
            linked_name='',
        )

    @classmethod
    def for_blog_article(cls, blog_article):
        return cls(
            blog_article=blog_article,
            linked_type=ActivityLinkType.BLOG_ARTICLE,
            linked_name=blog_article.title,
        )

    @classmethod
    def for_material_model(cls, material_model):
        return cls(
            material_model=material_model,
            linked_type=ActivityLinkType.MATERIAL_MODEL,
            linked_name=material_model.name,
        )

    @classmethod
    def for_material_items(cls, material_items):
        activity_link = cls(linked_type=ActivityLinkType.MATERIAL_ITEMS)
        activity_link.save()
        activity_link.material_items.set(material_items)
        return activity_link


class ActivityManager(models.Manager):

    def create(self, **kwargs):
        defaults = {'details': {}}
        defaults.update(kwargs)
        return super().create(**defaults)


# Examples of activities vocabulary
# ┌actor┐ ┌action_type─────┐ ┌object────┐    ┌target──┐
# │John │ │posted comment  │ │comment 42│ to │event 47│
# │John │ │modified comment│ │comment 42│ in │event 47│
# │John │ │created event   │ │event 47  │    │        │
# └─────┘ └────────────────┘ └──────────┘    └────────┘
class Activity(models.Model):
    """Activity represents some action done by Person.

    For example:
    John posted comment 42 to event 47.
    """

    class Meta:
        db_table = 'activity'

    objects = ActivityManager()

    actor = models.ForeignKey(Person, on_delete=models.PROTECT)
    action_type = EnumField(ActivityType, max_length=32)

    # object_link and target_link is nullable, since activity imported from old site does not have all links
    # as some objects were deleted in the old site. related_name='+' used as we don't need reverse relations
    object_link = models.ForeignKey(ActivityLink, on_delete=models.PROTECT, null=True, blank=True, related_name='+')
    target_link = models.ForeignKey(ActivityLink, on_delete=models.PROTECT, null=True, blank=True, related_name='+')

    time = models.DateTimeField(default=timezone.now)

    # details is a dictionary and contains additional data for the activity.
    # The keys used depend on the action_type used.
    # Note that the detail fields may not be present at all in case the information is not available (such as for
    # activities imported from the old site).
    # Detail fields:
    #   page_slug: slug of the page (old site)
    #   state: new participation state
    #   content: source user content of the object (i.e. comment)
    #   old_content: old value of content in case of edits
    details = JSONField()

    def __repr__(self):
        return '<Activity ({!r}): {!r} {!r} {!r} {!r} {!r} {!r}>'.format(
            self.id, self.actor_id, self.action_type, self.object_link_id, self.target_link_id, self.time, self.details
        )

    def save(self, **kwargs):
        if self.object_link is not None:
            self.object_link.save()
            self.object_link_id = self.object_link.id
        if self.target_link is not None:
            self.target_link.save()
            self.target_link_id = self.target_link.id
        super().save(**kwargs)


class BlogArticle(models.Model):
    class Meta:
        db_table = 'blog_article'

    author = models.ForeignKey(Person, on_delete=models.PROTECT, verbose_name='Autor')
    created = models.DateTimeField(default=timezone.now, verbose_name='Vytvorené')
    modified = models.DateTimeField(blank=True, null=True, verbose_name='Upravené')
    title = models.CharField(max_length=255, verbose_name='Nadpis')
    content = FormattedTextField(verbose_name='Obsah')

    def get_absolute_url(self):
        return '{}#blog-{}'.format(reverse('blog_index'), self.id)


class MaterialCategory(models.Model):
    """Category of material, i.e. summer or winter"""

    class Meta:
        db_table = 'material_category'

    name = models.CharField(max_length=255, verbose_name='Názov')
    order = models.IntegerField(verbose_name='Poradie')

    def __str__(self):
        return self.name


def material_model_image_upload_path(material_model, filename):
    _, ext = os.path.splitext(filename)
    ext = ext.lower()
    if ext not in ('.jpg', '.jpeg', '.png'):
        ext = '.bin'
    return 'material/{}/{}{}'.format(material_model.id, secrets.token_hex(), ext)


class MaterialModel(models.Model):
    """Model of a material, i.e. Coleman RIVERSIDE 9 DELUXE tent"""

    class Meta:
        db_table = 'material_model'

    name = models.CharField(max_length=255, verbose_name='Názov')
    description = FormattedTextField(verbose_name='Popis', blank=True)
    category = models.ForeignKey(MaterialCategory, verbose_name='Kategória', on_delete=models.PROTECT,
                                 related_name='models')
    price_eur = models.DecimalField(max_digits=10, decimal_places=3, null=True, blank=True)
    can_borrow = models.BooleanField(default=True, verbose_name='Dá sa požičať')

    def get_absolute_url(self):
        return reverse('material_detail', args=(self.id,))

    def __str__(self):
        return self.name


class MaterialItem(models.Model):
    """Concrete physical item of a given model"""
    class Meta:
        db_table = 'material_item'

    model = models.ForeignKey(MaterialModel, on_delete=models.PROTECT, verbose_name='Model', related_name='items')
    commissioned_date = models.DateField(null=True, blank=True, verbose_name='Dátum zaradenia')
    decommissioned_date = models.DateField(null=True, blank=True, verbose_name='Dátum vyradenia')


class MaterialNotAvailable(Exception):
    def __init__(self, material_count, message):
        self.material_count = material_count
        self.message = message


class LoansNotAvailable(Exception):
    def __init__(self, loan_count, message):
        self.loan_count = loan_count
        self.message = message


class NothingToReturn(Exception):
    pass


class MaterialLoanManager(models.Manager):
    @staticmethod
    def _select_available_items_query(models_counts: Dict[int, int],
                                      during: psycopg2.extras.DateTimeRange):
        """Return SQL query that returns only available items in the given time interval.

        :param models_counts: a dictionary of {model_id: item_count}
        :param during: date time range when to reserve
        """
        if not models_counts:
            raise ValueError('No model specified')

        sql_parts = []
        params = []
        for model_id, count in models_counts.items():
            sql_parts.append(
                '(SELECT i.id, i.model_id, i.commissioned_date, i.decommissioned_date '
                'FROM material_item i '
                'WHERE i.model_id = %s '
                'AND (decommissioned_date IS NULL OR decommissioned_date >= upper(%s)) '
                'AND NOT EXISTS (SELECT l.id FROM material_loan l WHERE l.item_id = i.id AND l.during && %s) '
                'LIMIT %s)')
            params.extend([model_id, during, during, count])
        return ' UNION '.join(sql_parts), params

    def lend_models(self, models_counts: Iterable[Tuple[MaterialModel, int]], during: psycopg2.extras.DateTimeRange,
                    loaned_to: Person):
        """Try to reserve given count of models in the given datetime range

        :param models_counts: an iterable of (model, count) tuples
        :param during: date time range when to reserve
        :param loaned_to: person to which to lend the items
        :return: list of item_id that were registered
        """
        from django.db import connection

        dict_counts = {}
        for model, item_count in models_counts:
            if model.pk in dict_counts:
                raise ValueError('Duplicate item count for model {} detected'.format(model))
            dict_counts[model.pk] = item_count

        available_sql, available_params = self._select_available_items_query(dict_counts, during)

        with transaction.atomic(), connection.cursor() as cursor:
            # Select available items
            query = 'CREATE TEMPORARY TABLE loan_items AS ' + available_sql
            params = available_params
            cursor.execute(query, params)

            # Check we got all that we requested
            query = 'SELECT model_id, count(id) FROM loan_items GROUP BY model_id'
            cursor.execute(query)

            unavailable_material = {}
            for model_id, item_count in cursor:
                if dict_counts[model_id] != item_count:
                    unavailable_material[model_id] = dict_counts[model_id] - item_count

            if unavailable_material:
                raise MaterialNotAvailable(unavailable_material, 'Required amount of material not available')

            # Try to create a reservation
            query = 'INSERT INTO material_loan (item_id, during, loaned_to_id) ' \
                    'SELECT id, %s, %s FROM loan_items'
            params = [during, loaned_to.id]
            cursor.execute(query, params)

            return MaterialItem.objects.raw('SELECT id, model_id, commissioned_date, decommissioned_date '
                                            'FROM loan_items')

    def return_models(self, item_spec: Iterable[Tuple[MaterialModel, psycopg2.extras.DateRange, int]],
                      loaned_to: Person):
        """Try to reserve given count of models in the given datetime range

        :param item_spec: an iterable of (model, during, count) tuples
        :param loaned_to: person to which to lend the items
        :return: list of item_id that were returned
        """
        from django.db import connection

        dict_counts = {}
        total_count = 0
        for model, during, loan_count in item_spec:
            key = (model.pk, date_range_exclusive(during))
            if key in dict_counts:
                raise ValueError('Duplicate item count for model {} during {} detected'.format(model, during))
            dict_counts[key] = loan_count
            total_count += loan_count

        if total_count == 0:
            raise NothingToReturn('No material to be returned')

        with transaction.atomic(), connection.cursor() as cursor:
            # Select loans to return
            cursor.execute('CREATE TEMPORARY TABLE return_loans ('
                           'id INTEGER NOT NULL, '
                           'item_id INTEGER NOT NULL, '
                           'during daterange NOT NULL, '
                           'model_id INTEGER NOT NULL)')
            for (model_id, during), count in dict_counts.items():
                if count == 0:
                    continue

                cursor.execute(
                    'INSERT INTO return_loans SELECT l.id, l.item_id, l.during, i.model_id '
                    'FROM material_loan l '
                    'INNER JOIN material_item i ON l.item_id = i.id '
                    'WHERE i.model_id = %s '
                    'AND l.during = %s AND l.returned_time IS NULL AND l.loaned_to_id = %s'
                    'LIMIT %s '
                    'FOR UPDATE OF l',
                    (model_id, during, loaned_to.pk, count)
                )

            # Check we got all that we requested
            query = 'SELECT model_id, during, count(id) FROM return_loans GROUP BY model_id, during'
            cursor.execute(query)

            unavailable_loans = {}
            for model_id, during, loan_count in cursor:
                key = (model_id, during)
                if dict_counts[key] != loan_count:
                    unavailable_loans[key] = dict_counts[key] - loan_count

            if unavailable_loans:
                raise LoansNotAvailable(unavailable_loans, 'Required amount of material to return not available')

            today = timezone.now().date()
            tomorrow = today + datetime.timedelta(days=1)
            # Delete loans that are returned before they start
            cursor.execute('DELETE FROM material_loan WHERE %s < lower(during) AND id IN (SELECT id FROM return_loans)',
                           (today,))
            # Shorten loans that are returned after they start, but do not extend them (the latter might cause conflict)
            # Set as returned in any case
            cursor.execute('UPDATE material_loan SET during = daterange(lower(during), least(%s, upper(during))),'
                           'returned_time = now() '
                           'WHERE %s >= lower(during) AND id IN (SELECT id FROM return_loans)',
                           (tomorrow, today))

            return MaterialItem.objects.raw('SELECT item_id AS id, model_id FROM return_loans')


def has_bounds(r):
    if r.lower is None:
        raise ValidationError(
            'Interval nemá spodnú hranicu',
            code='invalid',
        )
    if r.upper is None:
        raise ValidationError(
            'Interval nemá hornú hranicu',
            code='invalid',
        )


def date_range_inclusive(r):
    """Normalize a date range bounds to [] instead of [) returned by the database"""
    if r is None:
        return None
    lower = r.lower
    upper = r.upper
    if lower and not r.lower_inc:
        lower += datetime.timedelta(days=1)
    if upper and not r.upper_inc:
        upper -= datetime.timedelta(days=1)
    return DateRange(lower=lower, upper=upper, bounds='[]')


def date_range_exclusive(r):
    """Normalize a date range bounds to [) to match those returned by the database"""
    if r is None:
        return None
    lower = r.lower
    upper = r.upper
    if lower and not r.lower_inc:
        lower += datetime.timedelta(days=1)
    if upper and r.upper_inc:
        upper += datetime.timedelta(days=1)
    return DateRange(lower=lower, upper=upper, bounds='[)')


class MaterialLoan(models.Model):
    class Meta:
        db_table = 'material_loan'

    objects = MaterialLoanManager()

    item = models.ForeignKey(MaterialItem, on_delete=models.PROTECT, verbose_name='Položka')
    during = DateRangeField(verbose_name='Obdobie pôžičky', validators=[has_bounds])
    loaned_to = models.ForeignKey(Person, on_delete=models.PROTECT, verbose_name='Požičané komu')
    returned_time = models.DateTimeField(null=True)


def all_user_content():
    """Returns a generator of all user content in the database.

    The generated values are tuples of (Model class, Model instance, field name)
    """
    for comment in Comment.objects.all():
        yield Comment, comment, 'content'
    for event in Event.objects.all():
        yield Event, event, 'description'
    for review in Review.objects.all():
        yield Review, review, 'content'
    for topic in Topic.objects.all():
        yield Topic, topic, 'description'
    for blog_article in BlogArticle.objects.all():
        yield BlogArticle, blog_article, 'description'
    for model in MaterialModel.objects.all():
        yield MaterialModel, model, 'description'
