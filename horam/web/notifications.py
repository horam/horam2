from email.headerregistry import Address

from django.core.mail import send_mail
from django.core import mail
from django.template.loader import render_to_string

from horam.web.activity_formatter import ActivityFormatter
from horam.web.models import EventNotificationLevel, NotificationSendLevel, ParticipationState, User


def new_event_notification_recipients():
    from django.db import connection

    with connection.cursor() as cursor:
        query = '''
            SELECT user_account.email, person.nickname
            FROM user_account
            INNER JOIN person ON person.user_id = user_account.id
            WHERE is_active AND new_event_notifications = true
        '''
        cursor.execute(query)
        for email, nickname in cursor:
            yield str(Address(nickname, addr_spec=email))


def event_notification_recipients(event, send_level: NotificationSendLevel):
    from django.db import connection

    with connection.cursor() as cursor:
        if send_level == NotificationSendLevel.FORCE:
            query = '''
                SELECT user_account.email, person.nickname
                FROM user_account
                INNER JOIN person ON person.user_id = user_account.id
                WHERE is_active AND event_notification_level <> %s
            '''
            cursor.execute(query, (EventNotificationLevel.NEVER.value,))
        elif send_level == NotificationSendLevel.DEFAULT:
            query = '''
                SELECT user_account.email, person.nickname
                    FROM user_account
                    INNER JOIN person ON person.user_id = user_account.id
                    WHERE is_active AND event_notification_level = %s
                UNION SELECT user_account.email, person.nickname
                    FROM user_account
                    INNER JOIN person ON person.user_id = user_account.id
                    WHERE is_active AND event_notification_level = %s AND EXISTS (
                        SELECT id
                        FROM event_participation
                        WHERE person_id = person.id AND event_id = %s AND state = %s
                    )
                UNION SELECT user_account.email, person.nickname
                    FROM user_account
                    INNER JOIN person ON person.user_id = user_account.id
                    WHERE is_active AND event_notification_level = %s AND NOT EXISTS (
                        SELECT id
                        FROM event_participation
                        WHERE person_id = person.id AND event_id = %s AND state = %s
                    )
            '''
            cursor.execute(query,
                           (EventNotificationLevel.ALWAYS.value,
                            EventNotificationLevel.OPT_IN.value, event.id, ParticipationState.PARTICIPANT.value,
                            EventNotificationLevel.OPT_OUT.value, event.id, ParticipationState.NONPARTICIPANT.value))
        elif send_level == NotificationSendLevel.SUPPRESS:
            query = '''
                SELECT user_account.email, person.nickname
                FROM user_account
                INNER JOIN person ON person.user_id = user_account.id
                WHERE is_active AND event_notification_level = %s
            '''
            cursor.execute(query, (EventNotificationLevel.ALWAYS.value,))

        for email, nickname in cursor:
            yield str(Address(nickname, addr_spec=email))


def topic_notification_recipients(topic):
    qs = User.objects.filter(is_active=True, topic_notifications=True)\
        .select_related('person')\
        .values_list('email', 'person__nickname')
    for email, nickname in qs:
        yield str(Address(nickname, addr_spec=email))


def send_notification(activity, recipients, request, subject):
    email_context = {
        'activity': activity,
        'subject': subject,
        'activity_formatter': ActivityFormatter(url_transform=request.build_absolute_uri),
    }
    with mail.get_connection() as connection:
        for recipient in recipients:
            text_body = render_to_string('emails/activity_notification.txt', context=email_context)
            html_body = render_to_string('emails/activity_notification.html', context=email_context)
            message = mail.EmailMultiAlternatives(
                subject=subject,
                body=text_body,
                from_email=None,
                to=[recipient],
                connection=connection
            )
            message.attach_alternative(html_body, 'text/html')
            message.send()


def send_event_notification(request, event, activity, recipients):
    subject = '[horam] {}'.format(event.name)
    send_notification(activity, recipients, request, subject)


def send_topic_notification(request, topic, activity, recipients):
    subject = '[horam] {}'.format(topic.title)
    send_notification(activity, recipients, request, subject)
