def can_edit_user(user, other_user):
    if not user.is_authenticated:
        return False
    return user.is_staff or user == other_user


def can_deactivate(user, other_user):
    """Return True if self can deactivate other_user"""
    if not user.is_authenticated:
        return False
    return user.is_staff and user != other_user


def can_create_event(user):
    return user.is_authenticated


def can_edit_event(user, event):
    if not user.is_authenticated:
        return False
    return user.is_staff or user.person in event.organizers.all()


def can_edit_topic(user, topic):
    if not user.is_authenticated:
        return False
    return user.is_staff or topic.author == user.person


def can_edit_comment(user, comment):
    if not user.is_authenticated:
        return False
    return user.person == comment.author


def can_upload_to_gallery(user, event):
    return user.is_authenticated


def can_create_blog(user):
    return user.is_authenticated and user.is_staff


def can_edit_blog(user, blog_article):
    return can_create_blog(user)


def can_create_material(user):
    return user.is_authenticated and user.is_staff


def can_edit_material(user, material_model):
    return can_create_material(user)


def can_borrow_material(user):
    return user.is_authenticated
