"use strict";
$(function() {
  var calendarEl = document.getElementById("event-calendar");

  var calendar = new FullCalendar.Calendar(calendarEl, {
      initialView: 'dayGridMonth',
      locale: 'sk',
      height: '100%',
      events: function(fetchInfo, successCallback, failureCallback) {
          $.ajax({
              url: '/akcie/kalendar.json',
              dataType: 'json',
              data: {
                start: fetchInfo.startStr,
                end: fetchInfo.endStr
              },
              success: function(data) {
                  var events = [];
                  for (var i=0; i < data.events.length; i++) {
                      var sourceEvent = data.events[i];
                      // https://fullcalendar.io/docs/event-object
                      var event = {
                          id: sourceEvent.id,
                          title: sourceEvent.name,
                          start: sourceEvent.date_start,
                          end: sourceEvent.date_end_exclusive,
                          url: sourceEvent.url,
                          className: 'calendar-event-state-' + sourceEvent.state.toLowerCase()
                      };
                      events.push(event);
                  }
                  successCallback(events);
              },
              error: function() {
                  failureCallback();
              }
            });
      }
  });

  calendar.render();

  // the below events fix resizing of calendar to fit content when the content box changes.

  // the animation in css is set to 500 ms, this triggers around the time it's finished.
  $('body').on('menu-change', function() {
      setTimeout(function() {
          calendar.render();
      }, 500);
  });

  // this triggers exactly when the menu animation finishes.
  $('#main').one("animationend webkitAnimationEnd oAnimationEnd MSAnimationEnd", function(){
      calendar.render();
  });

  // in modern browsers, we can use resize observer to watch for changes of the content box itself.
  if (window.ResizeObserver) {
    let ro = new ResizeObserver( entries => {
        calendar.render();
    });
    ro.observe(document.querySelector('.content'));
  }
});