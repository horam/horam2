"use strict";

function formatPerson (person) {
  if (!person.id) {
    return person.text;
  }

  var avatar = document.createElement('div');
  avatar.className = 'avatar';

  if (person.avatar_thumbnail) {
      var img = document.createElement('img');
      img.src = person.avatar_thumbnail;
      avatar.appendChild(img);
  } else {
      avatar.style.backgroundColor = person.avatar_color;
      avatar.textContent = person.avatar_initials;
  }

  var label = document.createElement('div');
  label.className = 'label';
  label.appendChild(document.createTextNode(person.text));

  var container = document.createElement('div');
  container.className = 'select2-organizers';
  container.appendChild(avatar);
  container.appendChild(label);

  return container;
}

$(document).ready(function() {
	$('#id_organizers').select2({
        templateResult: formatPerson,
        ajax: {
            url: '/select2/person',
            dataType: 'json',
            delay: 100,
        }
    });
});