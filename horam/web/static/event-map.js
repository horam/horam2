"use strict";
$(function() {
    var el = document.getElementById('event-map');
    var map = L.map(el).setView([48.170430, 17.096190], 4);

    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="https://osm.org/copyright">OpenStreetMap</a> contributors'
    }).addTo(map);

    // We need to invalidate map dimensions after a menu toggle, as leaflet does not detect the transition
    // (at least in Firefox)
    $(".menu").on('transitionend', function() {
        map.invalidateSize();
    });

    $.ajax('/akcie/mapa.geojson').done(function(data) {
        var clusterGroup = L.markerClusterGroup({
            maxClusterRadius: 20,
        });
        var features = L.geoJSON(data, {
            pointToLayer: function(feature, latlng) {
                var content = $('<div>');
                content.append($('<a>', {
                    class: 'event-name',
                    href: feature.properties.url
                }).text(feature.properties.name));

                var marker = L.marker(latlng, {
                    title: feature.properties.name
                });

                marker.bindPopup(content[0]);

                return marker;
            }
        });
        clusterGroup.addLayer(features);
        map.addLayer(clusterGroup);
    }).fail(function(xhr, status, err) {
        addFlash('error', err);
    });
});