"use strict";
window.Dropzone.options.eventPhotosDropzone = {
    paramName: "image", // POST parameter name
    maxFilesize: 0.5, // MB
    acceptedFiles: "image/*",
    init: function() {
      this.on("success", function(file, response) {
        var newImg = document.createElement('img');
        newImg.src = URL.createObjectURL(file);
        $('#photo-list').append(newImg);
      });
    },
    /**
    * The text used before any files are dropped.
    */
    dictDefaultMessage: "Pretiahni obrázky sem alebo klikni pre výber.",

    /**
    * The text that replaces the default message text it the browser is not supported.
    */
    dictFallbackMessage: "Tvoj prehliadač nepodporuje upload drag and drop.",

    /**
    * The text that will be added before the fallback form.
    * If you provide a  fallback element yourself, or if this option is `null` this will
    * be ignored.
    */
    dictFallbackText: "Prosím použi formulár nižšie pre upload obrázku",

    /**
    * If the filesize is too big.
    * `{{filesize}}` and `{{maxFilesize}}` will be replaced with the respective configuration values.
    */
    dictFileTooBig: "Súbor je príliš veľký ({{filesize}}MiB). Maximálna veľkosť: {{maxFilesize}}MiB.",

    /**
    * If the file doesn't match the file type.
    */
    dictInvalidFileType: "Nemôžeš nahrávať súbory tohto typu.",

    /**
    * If the server response was invalid.
    * `{{statusCode}}` will be replaced with the servers status code.
    */
    dictResponseError: "Server odpovedal stavovým kódom {{statusCode}}.",

    /**
    * If `addRemoveLinks` is true, the text to be used for the cancel upload link.
    */
    dictCancelUpload: "Zrušiť upload",

    /**
    * The text that is displayed if an upload was manually canceled
    */
    dictUploadCanceled: "Upload zrušený.",

    /**
    * If `addRemoveLinks` is true, the text to be used for confirmation when cancelling upload.
    */
    dictCancelUploadConfirmation: "Naozaj chceš zrušiť upload?",

    /**
    * If `addRemoveLinks` is true, the text to be used to remove a file.
    */
    dictRemoveFile: "Odstrániť súbor",

    /**
    * If this is not null, then the user will be prompted before removing a file.
    */
    dictRemoveFileConfirmation: null,

    /**
    * Displayed if `maxFiles` is st and exceeded.
    * The string `{{maxFiles}}` will be replaced by the configuration value.
    */
    dictMaxFilesExceeded: "Nemôžeš nahrávať viac súborov.",

};