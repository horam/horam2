"use strict";

function ifEmpty(text, defaultText) {
    if (!text || text.length === 0) {
        return defaultText;
    }
    return text;
}

function replaceSelection(element, replacer) {
    //IE support
    var replacement;
    if (document.selection) {
        element.focus();
        var sel = document.selection.createRange();
        replacement = replacer(sel.text);
        sel.text = replacement.text;
    }

    //MOZILLA/NETSCAPE support
    else if (element.selectionStart || element.selectionStart === 0) {
        var startPos = Math.min(element.selectionStart, element.selectionEnd);
        var endPos = Math.max(element.selectionStart, element.selectionEnd);
        var content = element.value.substring(startPos, endPos);
        replacement = replacer(content);
        element.value = element.value.substring(0, startPos)
            + replacement.text
            + element.value.substring(endPos, element.value.length);
        element.focus();
        var newStart = startPos + replacement.selStartOffset;
        var newEnd = startPos + replacement.selEndOffset;
        element.setSelectionRange(newStart, newEnd);
    } else {
        replacement = replacer('');
        element.value += replacement.text;
    }
}

function encloseSelection(element, before, defaultText, after) {
    replaceSelection(element, function (sel) {
        var middle = ifEmpty(sel, defaultText);
        return {
            text: before + middle + after,
            selStartOffset: before.length,
            selEndOffset: before.length + middle.length
        };
    });
}

function formattedEditArea(editArea, initialContent) {
    var $editArea = $(editArea);
    var form = editArea.form;

    var next = editArea.nextSibling;
    var parent = editArea.parentNode;
    var wrapper = document.createElement('div');
    wrapper.setAttribute('class', 'editarea-wrapper');
    parent.removeChild(editArea);

    // create a new edit area with slightly different text
    // we'll copy it back to the original text area on submit
    var editArea2 = document.createElement('textarea');
    var $editArea2 = $(editArea2);
    $.each($editArea.prop("attributes"), function() {
        if (this.name !== 'name' && this.name !== 'id') {
            $editArea2.attr(this.name, this.value);
        }
    });
    editArea2.value = initialContent;
    form.addEventListener("submit", function() {
        editArea.value = "oldhoram\n" + editArea2.value;
        return true;
    });
    $editArea.addClass("shadow-textarea");

    function encloseButton(caption, before, defaultText, after) {
        var button = document.createElement('input');
        button.setAttribute('type', 'button');
        button.setAttribute('value', caption);
        button.addEventListener('click', function (event) {
            encloseSelection(editArea, before, defaultText, after);
        });
        return button
    }

    var buttons = document.createElement('div');
    buttons.setAttribute('class', 'buttons');
    buttons.appendChild(encloseButton('B', '[b]', '', '[/b]'));
    buttons.appendChild(encloseButton('U', '[u]', '', '[/u]'));
    buttons.appendChild(encloseButton('I', '[i]', '', '[/i]'));
    buttons.appendChild(encloseButton('H1', '[h1]', '', '[/h1]'));
    buttons.appendChild(encloseButton('H2', '[h2]', '', '[/h2]'));
    buttons.appendChild(encloseButton('H3', '[h3]', '', '[/h3]'));
    buttons.appendChild(encloseButton('Img', '[img]', 'https://', '[/img]'));
    buttons.appendChild(encloseButton('URL', '[url=https://]', 'link', '[/url]'));
    buttons.appendChild(encloseButton('center', '[center]', '', '[/center]'));
    wrapper.appendChild(buttons);
    wrapper.appendChild(editArea);
    wrapper.appendChild(editArea2);
    if (next) {
        parent.insertBefore(wrapper, next);
    }
    else {
        parent.appendChild(wrapper);
    }
}

$(document).ready(function () {
    $("textarea.formatted-content").each(function (index) {
        var oldhoram = this.value.match(/^oldhoram\r?\n([^]*)$/m);
        if (oldhoram) {
            formattedEditArea(this, oldhoram[1]);
            return;
        }
        new EasyMDE({
            element: this,
            insertTexts: {
                image: ["![popis obrázku](https://adresa", ")"],
                link: ["[popis linku", "](https://adresa)"],
            },
            status: false,
            forceSync: true
        });

        // Fix browser form validation (it does not work on hidden textarea)
        $(this).prop('required', false);
    })
});

$(document).ready(function () {
    $("div.map").each(function (index) {
        var lat = $(this).attr('data-lat');
        var lon = $(this).attr('data-lon');
        var zoom = $(this).attr('data-zoom') || 16;
        var map = L.map(this).setView([lat, lon], zoom);

        L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
        }).addTo(map);

        L.marker([lat, lon]).addTo(map);
    });
});

$(document).ready(function () {
    $("td.activity-with-detail").each(function (index) {
        console.log('AA');
        var details = $(this).find('.activity-detail');
        var moreLink = document.createElement('a');
        moreLink.href = '#';
        moreLink.textContent = '(viac)';
        moreLink.addEventListener('click', function (event) {
            if (details.hasClass('expanded')) {
                details.removeClass('expanded');
                moreLink.textContent = '(viac)';
            } else {
                details.addClass('expanded');
                moreLink.textContent = '(menej)';
            }
        });
        this.appendChild(moreLink);
    });
});

function addFlash(severity, message) {
    var flash = $('.flash');
    if (flash.length === 0) {
        flash = $('<aside class="flash"><ul></ul></aside>');
        $('.content-wrapper').prepend(flash);
    }
    var li = $('<li/>', {class: severity});
    li.text(message);
    li.click(function() {
        $(this).remove();
    });
    flash.find('ul').append(li);
}

$(document).ready(function () {
    $("div.comment-add.comment-box").each(function (index) {
        var $this = $(this);
        $this.addClass("suppressed");
        var showButton = $('<button></button>', {class: 'unsuppress material-button'});
        showButton.click(function() {
            $this.removeClass("suppressed");
        });
        showButton.text($this.find('button').text());
        $this.append(showButton);
    });

    $(".menu-toggle").click(function(evt) {
        var body = $('body');
        if (body.hasClass('menu-default')) {
            var menu = $('.menu');
            if (menu.width() > 0) {
                body.addClass('menu-hidden');
            }
            body.removeClass('menu-default');

        } else {
            body.toggleClass('menu-hidden');
        }
        body.trigger('menu-change');
    });

    $(".content-shade").click(function(evt) {
        $('body').addClass('menu-hidden').removeClass('menu-default').trigger('menu-change');
    });
});

$(document).ready(function() {
    $("aside.flash").click(function() {
        $(this).remove();
    });
});