"use strict";

function checkAppendRow() {
    if ($('#cart-content select').last().val() !== "") {
        var totalFormsField = $("#id_form-TOTAL_FORMS");
        var totalForms = parseInt(totalFormsField.val());
        totalFormsField.val(totalForms + 1);

        var newRow = $('#cart-content tr').first().clone();
        newRow.find('#id_form-0-model').change(checkAppendRow).attr("id", "id_form-" + totalForms + "-model")
            .attr("name", "form-" + totalForms + "-model");
        newRow.find("#id_form-0-count").attr("id", "id_form-" + totalForms + "-count")
            .attr("name", "form-" + totalForms + "-count");
        newRow.appendTo($('#cart-content tbody'));
    }
}

$(document).ready(function() {
    $('#cart-content select').change(checkAppendRow)
});