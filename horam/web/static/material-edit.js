"use strict";

$(document).ready(function() {
    $('#select-all').change(function() {
        $(this).closest('table').find('td input[type=checkbox]').prop('checked', this.checked);
    })
});