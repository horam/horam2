Vendored libraries
------------------

This directory is exempted from the Django hashing of files for
static file cache busting.

This means that every directory here should have the version of the library
in the name at the end, so that users don't need to reload browser cache when
we upgrade the library.