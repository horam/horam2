import datetime
import difflib

from django import template
from django.utils import timezone
from django.utils.html import format_html_join, format_html

from horam.web import user_content
from horam.web.activity_formatter import ActivityFormatter
from horam.web.models import date_range_inclusive

register = template.Library()


@register.filter(name='format_date')
def format_date(d):
    if d is None:
        return None
    return d.strftime('%d.%m.%Y')


@register.filter(name='format_days')
def format_days(days):
    if days is None:
        return None
    if days == 1:
        return '1 deň'
    elif 2 <= days <= 4:
        return '{} dni'.format(days)
    else:
        return '{} dní'.format(days)


@register.filter(name='join_attr')
def join_attr(items, attribute_name, joiner=', '):
    attributes = (getattr(x, attribute_name, None) for x in items)
    return joiner.join(filter(lambda x: x is not None, attributes))


@register.filter(name='human_date', expects_localtime=True)
def human_date(dt):
    if (datetime.datetime.utcnow().replace(tzinfo=datetime.timezone.utc) - dt) < datetime.timedelta(days=1):
        return dt.strftime('%H:%M')
    else:
        return dt.strftime('%d.%m.%Y %H:%M')


@register.filter(name='conditional_year', expects_localtime=True)
def conditional_year(dt):
    if dt.year == timezone.localtime().year:
        return dt.strftime('%d.%m. %H:%M')
    else:
        return dt.strftime('%d.%m.%Y %H:%M')


@register.simple_tag
def render_user_content(source_text):
    """Return a HTML rendered version of source text"""
    return user_content.render_user_content(source_text)


@register.simple_tag
def activity_description(activity, formatter=None, as_text=False):
    if formatter is None:
        formatter = ActivityFormatter()

    if as_text:
        return formatter.text(activity)
    else:
        return formatter.html(activity)


@register.simple_tag
def activity_links(activity, formatter=None, as_text=False):
    if formatter is None:
        formatter = ActivityFormatter()

    links = []
    for token in formatter.tokens(activity):
        try:
            links.append(token.url)
        except AttributeError:
            pass

    if as_text:
        return '\n'.join(links)
    else:
        return format_html('<ul>{}</ul>', format_html_join('', '<li>{}</li>', ((link,) for link in links)))


@register.simple_tag
def activity_detail(activity, as_text=False):
    content = activity.details.get('content')
    old_content = activity.details.get('old_content')

    if content and old_content:
        # Show diff
        from_lines = old_content.splitlines()
        to_lines = content.splitlines()
        diff = '\n'.join(difflib.unified_diff(from_lines, to_lines, lineterm=''))

        if as_text:
            return diff
        else:
            return format_html('<pre>{}</pre>', diff)
    elif content:
        # Show content
        if as_text:
            return content
        else:
            return render_user_content(content)
    else:
        # Nothing to show
        return ''


@register.filter(name='email_text_quote')
def email_text_quote(text):
    """Prepend every line in text with '> '"""
    return '\n'.join('> {}'.format(line) for line in text.splitlines())


@register.filter(name='format_date_range')
def format_date_range(r):
    if r is None:
        return None
    normalized = date_range_inclusive(r)
    days = (normalized.upper - normalized.lower).days + 1

    if normalized.upper and normalized.lower != normalized.upper:
        return '{} - {} ({})'.format(format_date(normalized.lower), format_date(normalized.upper), format_days(days))
    else:
        return '{} ({})'.format(format_date(normalized.lower), format_days(days))
