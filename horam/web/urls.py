from django.urls import path
from django.views.generic import RedirectView, TemplateView

from horam.web import views
from horam.web.views import OldEventRedirectView, OldTopicRedirectView

urlpatterns = [
    path('', views.index, name='index'),

    # Akcie
    path('akcie/', views.events_index, name='events_index'),
    path('akcie/zaznamy', views.events_past, name='events_past'),
    path('akcie/kalendar', views.events_calendar, name='events_calendar'),
    path('akcie/kalendar.json', views.events_calendar_json, name='events_calendar_json'),
    path('akcie/kalendar.ics', views.events_calendar_ics, name='events_calendar_ics'),
    path('akcie/mapa', views.events_map, name='events_map'),
    path('akcie/mapa.geojson', views.events_map_geojson, name='events_map_geojson'),
    path('akcie/<int:event_id>-<event_slug>/', views.events_detail, name='events_detail'),
    path('akcie/<int:event_id>-<event_slug>/moja-ucast', views.events_my_participation, name='events_my_participation'),
    path('akcie/pridat', views.events_edit, name='events_add'),
    path('akcie/<int:event_id>-<event_slug>/upravit', views.events_edit, name='events_edit'),
    path('akcie/<int:event_id>-<event_slug>/komentare/pridat', views.events_edit_comment, name='events_add_comment'),
    path('akcie/<int:event_id>-<event_slug>/komentare/<int:comment_id>/upravit', views.events_edit_comment,
         name='events_edit_comment'),
    path('akcie/<int:event_id>-<event_slug>/zaznam', views.events_edit_review, name='events_edit_review'),
    path('akcie/<int:event_id>-<event_slug>/fotogaleria', views.events_photos, name='events_photos'),

    # Forum
    path('forum/', views.topics_index, name='topics_index'),
    path('forum/tema/pridat', views.topics_edit, name='topics_add'),
    path('forum/tema/<int:topic_id>-<topic_slug>', views.topics_detail, name='topics_detail'),
    path('forum/tema/<int:topic_id>-<topic_slug>/upravit', views.topics_edit, name='topics_edit'),
    path('forum/tema/<int:topic_id>-<topic_slug>/komentare/pridat', views.topics_edit_comment, name='topics_add_comment'),
    path('forum/tema/<int:topic_id>-<topic_slug>/komentare/<int:comment_id>/upravit', views.topics_edit_comment,
         name='topics_edit_comment'),

    # Clenovia
    path('clenovia', views.members_index, name='members_index'),
    path('clenovia/<username>', views.members_detail, name='members_detail'),
    path('clenovia/<username>/upravit', views.members_edit, name='members_edit'),
    path('clenovia/<username>/deaktivovat', views.members_deactivate, name='members_deactivate'),
    path('select2/person', views.persons_select2_data),

    # Aktivita
    path('aktivity', views.activities_index, name='activities_index'),

    # Blog
    path('novinky', views.blog_index, name='blog_index'),
    path('novinky/pridat', views.blog_edit, name='blog_add'),
    path('novinky/<int:article_id>/upravit', views.blog_edit, name='blog_edit'),

    # Material
    path('material', views.material_index, name='material_index'),
    path('material/vsetko', views.material_index_all, name='material_index_all'),
    path('material/pridat', views.material_edit, name='material_add'),
    path('material/<int:material_model_id>', views.material_detail, name='material_detail'),
    path('material/<int:material_model_id>/upravit', views.material_edit, name='material_edit'),
    path('material/<int:material_model_id>/zaradit', views.material_commission, name='material_commission'),
    path('material/<int:material_model_id>/vyradit', views.material_decommission, name='material_decommission'),
    path('material/kosik', views.material_cart, name='material_cart'),
    path('material/kosik/pridat', views.material_add_to_cart, name='material_add_to_cart'),
    path('material/moje-pozicky', views.material_my_loans, name='material_my_loans'),
    path('material/pozicky', views.material_loans, name='material_loans'),

    # Staticke stranky
    path('o-nas', views.pages_onas, name='pages_onas'),
    path('stanovy', TemplateView.as_view(template_name='pages/stanovy.html'), name='pages_stanovy'),
    path('zaujimave-odkazy', TemplateView.as_view(template_name='pages/zaujimave-odkazy.html'),
         name='pages_zaujimave_odkazy'),
    path('dve-percenta', TemplateView.as_view(template_name='pages/dve-percenta.html'), name='pages_dve_percenta'),
    path('kontakt', TemplateView.as_view(template_name='pages/kontakt.html'), name='pages_kontakt'),
    path('devinska37', TemplateView.as_view(template_name='pages/devinska37/2025.html'), name='pages_devinska37'),
    path('devinska37/historia', TemplateView.as_view(template_name='pages/devinska37/historia.html'),
         name='pages_devinska37_historia'),

    # Kompatibilita so starou strankou (url=None je HTTP 410 Gone)
    path('page/view/onas', RedirectView.as_view(pattern_name='pages_onas')),
    path('page/view/novinky', RedirectView.as_view(pattern_name='blog_index')),
    path('page/view/materil', RedirectView.as_view(pattern_name='material_index')),
    path('page/view/stanovy', RedirectView.as_view(pattern_name='pages_stanovy')),
    path('page/view/zaujimav-linky', RedirectView.as_view(pattern_name='pages_zaujimave_odkazy')),
    path('page/view/2', RedirectView.as_view(pattern_name='pages_dve_percenta')),
    path('page/view/devnska37', RedirectView.as_view(pattern_name='pages_devinska37')),
    path('page/view/devinska-37-historia', RedirectView.as_view(pattern_name='pages_devinska37_historia')),
    path('page/view/kontakt', RedirectView.as_view(pattern_name='pages_kontakt')),
    path('page/view/fotohelp', RedirectView.as_view(url=None)),
    path('page/edit/<page>', RedirectView.as_view(url=None)),
    path('page/delete/<page>', RedirectView.as_view(url=None)),
    path('page/add', RedirectView.as_view(url=None)),
    path('event/view_all/plan', RedirectView.as_view(pattern_name='events_index')),
    path('event/view_all/review', RedirectView.as_view(pattern_name='events_past')),
    path('event/view_all', RedirectView.as_view(url=None)),
    path('event/calendar', RedirectView.as_view(pattern_name='events_calendar')),
    path('event/icalendar', RedirectView.as_view(pattern_name='events_calendar_ics')),
    path('event/view/<int:event_id>', OldEventRedirectView.as_view(pattern_name='events_detail')),
    path('event/view/<int:event_id>/description', OldEventRedirectView.as_view(pattern_name='events_detail')),
    path('event/view/<int:event_id>/review', OldEventRedirectView.as_view(pattern_name='events_detail')),
    path('event/view/<int:event_id>/attendance', OldEventRedirectView.as_view(pattern_name='events_detail')),
    path('event/view/<int:event_id>/comments', OldEventRedirectView.as_view(pattern_name='events_detail')),
    path('event/view/<int:event_id>/photogallery', OldEventRedirectView.as_view(pattern_name='events_photos')),
    path('event/edit/<int:event_id>', OldEventRedirectView.as_view(pattern_name='events_edit')),
    path('event/delete/<int:event_id>', RedirectView.as_view(url=None)),
    path('user', RedirectView.as_view(pattern_name='members_index')),
    path('user/view_all', RedirectView.as_view(pattern_name='members_index')),
    # TODO: user/add
    path('user/view/<username>', RedirectView.as_view(pattern_name='members_detail')),
    path('user/edit/<username>', RedirectView.as_view(pattern_name='members_edit')),
    path('email', RedirectView.as_view(url=None)),
    path('email/view_all', RedirectView.as_view(url=None)),
    path('email/add', RedirectView.as_view(url=None)),
    path('topic', RedirectView.as_view(pattern_name='topics_index')),
    path('topic/view_all', RedirectView.as_view(pattern_name='topics_index')),
    path('topic/add', RedirectView.as_view(pattern_name='topics_add')),
    path('topic/view/<int:topic_id>', OldTopicRedirectView.as_view(pattern_name='topics_detail')),
    path('topic/edit/<int:topic_id>', OldTopicRedirectView.as_view(pattern_name='topics_edit')),
    path('topic/delete/<int:topic_id>', RedirectView.as_view(url=None)),
    path('comment/view/<int:comment_id>', views.redirect_old_comment_view),
    path('comment/delete/<int:comment_id>', RedirectView.as_view(url=None)),
    path('comment/delete_subcomment/<int:comment_id>', RedirectView.as_view(url=None)),
    path('news', RedirectView.as_view(pattern_name='activities_index')),
    path('login', RedirectView.as_view(pattern_name='login')),
    path('login/logout', RedirectView.as_view(pattern_name='logout')),
]
