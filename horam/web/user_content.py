import re
from abc import ABCMeta, abstractmethod

import bleach
import mistune
from bleach import html5lib_shim
from django.utils.html import escape, format_html
from django.utils.safestring import mark_safe
from urllib.parse import urlparse

from horam.md_renderer import MdRenderer

LEGACY_TOKENS = [
    ('url', r'\[url=(?P<url_href>[^\]]*)\](?P<url_text>[^\[]*)\[/url]'),
    ('img', r'\[img\](?P<img_src>[^\[]*)\[/img\]'),
    ('map', r'\[map](?P<map_lat>\d+(?:\.\d+)),(?P<map_lon>\d+(?:\.\d+))\[/map]'),
    ('br', r'(?:\r\n?|\n)'),
    ('nbsp', r'  ')
]

for tag in ['b', 'u', 'i', 'h1', 'h2', 'h3', 'center']:
    LEGACY_TOKENS.append((tag, rf'\[{tag}]'))
    LEGACY_TOKENS.append((f'{tag}_end', rf'\[/{tag}]'))

legacy_tokens_re = re.compile('|'.join('(?P<%s>%s)' % pair for pair in LEGACY_TOKENS))
legacy_re = re.compile(r'\Aoldhoram\r?\n(.*)\Z', re.MULTILINE | re.DOTALL)


ALLOWED_TAGS = bleach.ALLOWED_TAGS + [
    'center', 'div', 'img', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'u', 'br', 'p', 'figure', 'figcaption',
]
ALLOWED_ATTRIBUTES = bleach.ALLOWED_ATTRIBUTES.copy()
ALLOWED_ATTRIBUTES.update({
    'div': ['class', 'data-lon', 'data-lat'],
    'img': ['src', 'title', 'alt']
})


class ContentFilter:
    def link(self, href):
        return href

    def image(self, src):
        return src


class UserContent(metaclass=ABCMeta):
    @abstractmethod
    def render_html(self):
        pass

    @abstractmethod
    def filter(self, content_filter: ContentFilter):
        pass


class LegacyUserContent(UserContent):
    def __init__(self, raw_content):
        self.raw_content = raw_content

    def _tokenize(self):
        last_match_end = 0
        for match in legacy_tokens_re.finditer(self.raw_content):
            match_start, match_end = match.span()
            if match_start > last_match_end:
                yield ('text', self.raw_content[last_match_end:match_start])
            kind = match.lastgroup
            if kind in ('nbsp', 'br'):
                yield (kind,)
            elif kind in ('b', 'u', 'i', 'h1', 'h2', 'h3', 'center'):
                yield (kind, True)
            elif kind in ('b_end', 'u_end', 'i_end', 'h1_end', 'h2_end', 'h3_end', 'center_end'):
                yield (kind, False)
            elif kind == 'url':
                yield ('url', match['url_href'], match['url_text'])
            elif kind == 'img':
                yield ('img', match['img_src'])
            elif kind == 'map':
                yield ('map', match['map_lat'], match['map_lon'])
            else:
                raise ValueError(f'Unknown token kind: {kind}')
            last_match_end = match_end
        if last_match_end < len(self.raw_content):
            yield ('text', self.raw_content[last_match_end:])

    @staticmethod
    def _tokens_to_html(tokens):
        for token in tokens:
            kind = token[0]
            if kind == 'text':
                yield escape(token[1])
            elif kind == 'url':
                yield format_html('<a href="{href}" target="_blank">{text}</a>', href=token[1], text=token[2])
            elif kind == 'img':
                yield format_html('<img src="{src}" />', src=token[1])
            elif kind == 'map':
                yield format_html('<div class="map" data-lon="{lon}" data-lat="{lat}"></div>', lat=token[1],
                                  lon=token[2])
            elif kind in ('b', 'u', 'i', 'h1', 'h2', 'h3', 'center'):
                yield format_html('<{tag}>', tag=mark_safe(kind))
            elif kind in ('b_end', 'u_end', 'i_end', 'h1_end', 'h2_end', 'h3_end', 'center_end'):
                yield format_html('</{tag}>', tag=mark_safe(kind[:-4]))
            elif kind == 'br':
                yield mark_safe('<br />')
            elif kind == 'nbsp':
                yield mark_safe('&nbsp;&nbsp;')
            else:
                raise ValueError(f'Unknown token kind: {kind}')

    @staticmethod
    def _tokens_to_raw(tokens):
        for token in tokens:
            kind = token[0]
            if kind == 'text':
                yield token[1]
            elif kind == 'url':
                yield '[url={}]{}[/url]'.format(token[1], token[2])
            elif kind == 'img':
                yield '[img]{}[/img]'.format(token[1])
            elif kind == 'map':
                yield '[map]{},{}[/map]'.format(token[1], token[2])
            elif kind in ('b', 'u', 'i', 'h1', 'h2', 'h3', 'center'):
                yield '[{}]'.format(kind)
            elif kind in ('b_end', 'u_end', 'i_end', 'h1_end', 'h2_end', 'h3_end', 'center_end'):
                yield '[/{}]'.format(kind[:-4])
            elif kind == 'br':
                yield '\n'
            elif kind == 'nbsp':
                yield '  '
            else:
                raise ValueError(f'Unknown token kind: {kind}')

    def render_html(self):
        html = ''.join(self._tokens_to_html(self._tokenize()))
        html = bleach.Linker(recognized_tags=html5lib_shim.HTML_TAGS + ['center']).linkify(html)
        html = bleach.clean(html, tags=ALLOWED_TAGS, attributes=ALLOWED_ATTRIBUTES)
        return mark_safe(html)

    def filter(self, content_filter: ContentFilter):
        def apply_filter(tokens):
            for token in tokens:
                kind = token[0]
                if kind == 'text':
                    # TODO linkified content
                    yield token
                elif kind == 'url':
                    yield ('url', content_filter.link(token[1]), token[2])
                elif kind == 'img':
                    yield ('img', content_filter.image(token[1]))
                else:
                    yield token

        new_raw = ''.join(self._tokens_to_raw(apply_filter(self._tokenize())))

        # Ignore when only line endings (Windows/Unix) change
        if new_raw.splitlines() == self.raw_content.splitlines():
            return self

        return LegacyUserContent(new_raw)


LOCATION_RE = re.compile(r'^(?P<map_lat>\d+(?:\.\d+)),(?P<map_lon>\d+(?:\.\d+))$')


class HoramMarkdownRenderer(mistune.Renderer):

    def image(self, src, title, text):
        """Render markdown image to HTML.

        Horam version has a few changes in Markdown:
        - title is also rendered as figure caption
        - if URL uses map:/// URL scheme, render this as a map instead
        """

        if title:
            title = escape(title)

        if src.startswith('map:'):
            try:
                url = urlparse(src)
            except ValueError:
                content = '(invalid map url)'
            else:
                match = LOCATION_RE.match(url.path.lstrip('/'))
                if match:
                    latitude = match.group('map_lat')
                    longitude = match.group('map_lon')

                    content = format_html('<div class="map" data-lon="{lon}" data-lat="{lat}"></div>',
                                          lat=latitude, lon=longitude)
                else:
                    content = '(invalid map location)'
        else:
            src = mark_safe(mistune.escape_link(src))
            content = format_html('<img src="{src}" alt="{alt}"', src=src, alt=text)
            if title:
                content = format_html('{content} title="{title}"', content=content, title=title)

            if self.options.get('use_xhtml'):
                content = format_html('{content} />', content=content)
            else:
                content = format_html('{content}>', content=content)

        if title:
            return format_html('<figure>{content}<figcaption>{title}</figcaption></figure>',
                               content=content, title=title)

        return content


class HoramMarkdownFilter(MdRenderer):
    pass


class MarkdownUserContent(UserContent):
    def __init__(self, raw_content):
        self.raw_content = raw_content

    def render_html(self):
        html = mistune.markdown(self.raw_content, renderer=HoramMarkdownRenderer())
        html = bleach.clean(html, tags=ALLOWED_TAGS, attributes=ALLOWED_ATTRIBUTES)
        html = html.replace('<p></p>', '')  # Remove possible empty paragraphs caused by embedded figure or map div
        return mark_safe(html)

    def filter(self, content_filter: ContentFilter):
        filtered = mistune.markdown(self.raw_content, renderer=HoramMarkdownFilter())
        # make sure we have the same amount of endlines to prevent unrelated changes

        def newlines_at_end(s):
            m = re.search('(\n*)$', s)
            return len(m.group(1)) if m else 0

        wanted = newlines_at_end(self.raw_content)
        actual = newlines_at_end(filtered)
        if actual > wanted:
            filtered = filtered[:-(actual-wanted)]
        return MarkdownUserContent(filtered)


def load_raw_content(raw_content):
    legacy = legacy_re.match(raw_content)
    if legacy:
        return LegacyUserContent(legacy.group(1))

    return MarkdownUserContent(raw_content)


def dump_raw_content(content):
    if isinstance(content, LegacyUserContent):
        return 'oldhoram\n{}'.format(content.raw_content)
    return content.raw_content


def render_user_content(string):
    return load_raw_content(string).render_html()
