import collections
from copy import deepcopy

import icalendar
import iso8601
from django.contrib import messages
from django.contrib.auth import get_user_model
from django.contrib.auth.decorators import login_required
from django.contrib.sites.shortcuts import get_current_site
from django.core.exceptions import ValidationError
from django.core.paginator import Paginator
from django.db import transaction
from django.db.models import Count, Q, F
from django.http import HttpResponseRedirect, HttpResponseForbidden, HttpResponseNotFound, JsonResponse, HttpResponse, \
    Http404
from django.shortcuts import render, get_object_or_404
from django.urls import reverse
from django.utils import timezone
from django.utils.text import slugify
from django.views.generic import RedirectView

from horam.web.cart import MaterialCart
from horam.web.forms import EventForm, ReviewForm, CommentForm, TopicForm, ProfileForm, DeactivateForm, PhotoForm, \
    ParticipationForm, BlogArticleForm, MaterialModelForm, MaterialCommissionForm, MaterialDecommissionForm, \
    MaterialCartItemForm, MaterialCartLoanForm, MaterialReturnFormset
from horam.web.models import Event, EventState, Review, Discussion, Comment, Topic, Activity, Photo, ActivityType, \
    ActivityLink, get_extended_participation_state, set_extended_participation_state, NotificationSendLevel, Person, \
    BlogArticle, MaterialModel, MaterialItem, MaterialLoan, MaterialNotAvailable, LoansNotAvailable, \
    date_range_exclusive, NothingToReturn, Participation, ParticipationState
from horam.web.notifications import new_event_notification_recipients, event_notification_recipients, \
    send_event_notification, send_topic_notification, topic_notification_recipients
from horam.web.permissions import can_edit_event, can_edit_comment, can_edit_topic, can_edit_user, can_deactivate, \
    can_upload_to_gallery, can_edit_blog, can_create_blog, can_edit_material, can_create_material, can_borrow_material, \
    can_create_event


def index(request):
    if request.user.is_authenticated:
        return HttpResponseRedirect(reverse('activities_index'))
    else:
        return HttpResponseRedirect(reverse('pages_onas'))


def events_index(request):
    events = Event.objects.filter(state__in=(EventState.TENTATIVE, EventState.CONFIRMED))\
        .prefetch_related('organizers')\
        .order_by('date_start')
    return render(request, 'events/list.html', {
        'title': 'Plánované akcie',
        'add_new_link': can_create_event(request.user),
        'events': events,
    })


def events_past(request):
    events = Event.objects.filter(state__in=(EventState.CANCELLED, EventState.DONE))\
        .prefetch_related('organizers') \
        .annotate(review_count=Count('review')) \
        .order_by(F('date_start').desc(nulls_last=True))
    return render(request, 'events/list-review.html', {
        'title': 'Záznamy z akcií',
        'add_new_link': False,
        'events': events,
    })


def events_calendar(request):
    return render(request, 'events/calendar.html')


def events_calendar_json(request):
    """Return a JSON feed that will be displayed in JS calendar."""
    start = request.GET.get('start')
    end = request.GET.get('end')

    events = Event.objects.all()

    if start is not None and end is not None:
        start_date = iso8601.parse_date(start)
        end_date = iso8601.parse_date(end)

        events = events.extra(
            where=['(date_start, coalesce(date_start + duration_days * INTERVAL \'1 day\', date_start)) '
                   'overlaps (%s, %s)'],
            params=[start_date, end_date]
        )

    json_data = []
    for event in events:
        json_data.append({
            'id': event.id,
            'name': event.name,
            'date_start': event.date_start,
            'date_end': event.date_end,
            'date_end_exclusive': event.date_end_exclusive,
            'duration_days': event.duration_days,
            'url': event.get_absolute_url(),
            'state': event.state.name,
        })

    return JsonResponse({'events': json_data})


def events_calendar_ics(request):
    # https://icalendar.readthedocs.io/en/latest/usage.html
    cal = icalendar.Calendar()
    cal.add('prodid', '-//horam.sk//horam.sk//')
    cal.add('version', '2.0')
    cal.add('name', 'Horoklub Amatér')
    cal.add('x-wr-calname', 'Horoklub Amatér')
    cal.add('url', request.build_absolute_uri(reverse('events_calendar')))
    cal.add('source', request.build_absolute_uri(reverse('events_calendar_ics')))

    site = get_current_site(request)

    for event in Event.objects.filter(date_start__isnull=False):
        cal_event = icalendar.Event()
        cal_event.add('uid', 'event-{}@{}'.format(event.id, site.domain)),
        cal_event.add('summary', event.name)
        cal_event.add('dtstart', event.date_start)
        cal_event.add('url', request.build_absolute_uri(event.get_absolute_url()))
        if event.date_end_exclusive is not None:
            cal_event.add('dtend', event.date_end_exclusive)
        else:
            cal_event.add('dtend', event.date_start)
        if event.state in (EventState.CONFIRMED, EventState.DONE):
            cal_event.add('status', 'CONFIRMED')
        elif event.state == EventState.TENTATIVE:
            cal_event.add('status', 'TENTATIVE')
        elif event.state == EventState.CANCELLED:
            cal_event.add('status', 'CANCELLED')
        cal.add_component(cal_event)

    return HttpResponse(cal.to_ical(), content_type='text/calendar')


def events_map(request):
    return render(request, 'events/map.html')


def events_map_geojson(request):
    """Return a GeoJSON feed that will be displayed in map."""
    events = Event.objects.all().filter(latitude__isnull=False, longitude__isnull=False)

    features = []
    for event in events:
        features.append({
            'type': 'Feature',
            'geometry': {
                'type': 'Point',
                'coordinates': [event.longitude, event.latitude],
            },
            'properties': {
                'id': event.id,
                'name': event.name,
                'date_start': event.date_start,
                'date_end': event.date_end,
                'date_end_exclusive': event.date_end_exclusive,
                'duration_days': event.duration_days,
                'url': event.get_absolute_url(),
                'state': event.state.name,
            }
        })

    return JsonResponse({'type': 'FeatureCollection', 'features': features}, content_type='application/geo+json')


def events_detail(request, event_id, event_slug):
    event = get_object_or_404(Event, pk=event_id, slug=event_slug)

    def add_edit_url(comment):
        if can_edit_comment(request.user, comment):
            comment.edit_url = reverse('events_edit_comment', args=(event.id, event.slug, comment.id))

    if request.user.is_authenticated:
        comments = Comment.objects.discussion_tree(event.discussion_id, per_comment=add_edit_url)
        add_comment_url = reverse('events_add_comment', args=(event.id, event.slug))
        participation = get_extended_participation_state(event, request.user.person)
        participation_form = ParticipationForm(initial={'participation': participation})
        participants = []
        nonparticipants = []
        for p in Participation.objects.select_related('person').filter(event_id=event.id).order_by('person.nickname'):
            if p.state == ParticipationState.PARTICIPANT:
                participants.append(p.person)
            elif p.state == ParticipationState.NONPARTICIPANT:
                nonparticipants.append(p.person)
    else:
        comments = None
        add_comment_url = None
        participation_form = None
        participants = None
        nonparticipants = None

    return render(request, 'events/detail.html', {
        'event': event,
        'add_comment_url': add_comment_url,
        'comments': comments,
        'can_edit': can_edit_event(request.user, event),
        'participation_form': participation_form,
        'participants': participants,
        'nonparticipants': nonparticipants,
    })


@login_required
def events_my_participation(request, event_id, event_slug):
    event = get_object_or_404(Event, pk=event_id, slug=event_slug)

    if request.method == 'POST':
        participation_form = ParticipationForm(request.POST)
        if participation_form.is_valid():
            with transaction.atomic():
                set_extended_participation_state(event, request.user.person,
                                                 participation_form.cleaned_data['participation'])
                Activity.objects.create(
                    actor=request.user.person,
                    action_type=ActivityType.UPDATE_PARTICIPATION_STATE,
                    target_link=ActivityLink.for_event(event),
                    details={'state': participation_form.cleaned_data['participation'].value}
                )
            messages.add_message(request, messages.SUCCESS, 'Účasť upravená')
            return HttpResponseRedirect(event.get_absolute_url())
    else:
        participation = get_extended_participation_state(event, request.user.person)
        participation_form = ParticipationForm(initial={'participation': participation})

    return render(request, 'events/my-participation.html', {
        'form': participation_form,
        'event': event,
    })


@login_required
def events_edit(request, event_id=None, event_slug=None):
    initial = {}
    if event_id is not None:
        event = get_object_or_404(Event, pk=event_id, slug=event_slug)
        if not can_edit_event(request.user, event):
            return HttpResponseForbidden()
    else:
        event = Event(state=EventState.TENTATIVE)
        initial = {
            'state': EventState.TENTATIVE,
            'organizers': [request.user.person],
        }

    if request.method == 'POST':
        form = EventForm(request.POST, instance=event)
        old_event = deepcopy(event)
        if form.is_valid():
            with transaction.atomic():
                new_event = form.save(commit=False)
                if event_id is None:
                    # When adding new event, we need to create a discussion for it
                    discussion = Discussion()
                    discussion.save()
                    new_event.discussion = discussion
                    new_event.slug = slugify(new_event.name)
                    action_type = ActivityType.ADD_EVENT
                else:
                    action_type = ActivityType.UPDATE_EVENT
                new_event.save()
                form.save_m2m()
                activity_details = {}

                def encode_date(d):
                    if d is None:
                        return None
                    return d.isoformat()

                if event_id is not None:
                    if old_event.state != event.state:
                        activity_details['old_state'] = old_event.state.value
                        activity_details['state'] = event.state.value
                    if old_event.name != event.name:
                        activity_details['old_name'] = old_event.name
                        activity_details['name'] = event.name
                    if old_event.date_start != event.date_start:
                        activity_details['old_date_start'] = encode_date(old_event.date_start)
                        activity_details['date_start'] = encode_date(event.date_start)
                    if old_event.duration_days != event.duration_days:
                        activity_details['old_duration_days'] = old_event.duration_days
                        activity_details['duration_days'] = event.duration_days
                    if old_event.description != event.description:
                        activity_details['old_content'] = old_event.description
                        activity_details['content'] = event.description
                    if old_event.latitude != event.latitude or old_event.longitude != event.longitude:
                        activity_details['old_latitude'] = str(old_event.latitude)
                        activity_details['latitude'] = str(event.latitude)
                        activity_details['old_longitude'] = str(old_event.longitude)
                        activity_details['longitude'] = str(event.longitude)
                else:
                    activity_details['state'] = event.state.value
                    activity_details['date_start'] = encode_date(event.date_start)
                    activity_details['duration_days'] = event.duration_days
                    activity_details['content'] = event.description
                    activity_details['latitude'] = str(event.latitude)
                    activity_details['longitude'] = str(event.longitude)

                activity = Activity.objects.create(
                    actor=request.user.person,
                    action_type=action_type,
                    object_link=ActivityLink.for_event(new_event),
                    details=activity_details
                )
            if event_id is None:
                recipients = new_event_notification_recipients()
            else:
                recipients = event_notification_recipients(event, form.cleaned_data['notification_level'])
            send_event_notification(request, event, activity, recipients)
            messages.add_message(request, messages.SUCCESS, 'Akcia uložená')
            return HttpResponseRedirect(new_event.get_absolute_url())
    else:
        form = EventForm(instance=event, initial=initial)

    return render(request, 'events/edit.html', {
        'form': form,
    })


@login_required
def events_edit_comment(request, event_id, event_slug, comment_id=None):
    event = get_object_or_404(Event, pk=event_id, slug=event_slug)

    def on_success(activity):
        recipients = event_notification_recipients(event, NotificationSendLevel.DEFAULT)
        send_event_notification(request, event, activity, recipients)

    return edit_comment(request,
                        discussion_id=event.discussion_id,
                        success_url=event.get_absolute_url(),
                        comment_id=comment_id,
                        activity_target=ActivityLink.for_event(event),
                        on_success=on_success)


def edit_comment(request, discussion_id, success_url, comment_id=None, activity_target=None, on_success=None):
    if request.method == 'POST':
        form = CommentForm(request.POST)
        if form.is_valid():
            with transaction.atomic():
                activity_details = {}
                if comment_id is None:
                    if form.cleaned_data['parent'] is not None:
                        parent_comment = Comment.objects.get(pk=form.cleaned_data['parent'])
                        if parent_comment is None or parent_comment.discussion_id != discussion_id:
                            return HttpResponseForbidden()
                    comment = Comment(
                        parent_id=form.cleaned_data['parent'],
                        discussion_id=discussion_id,
                        author=request.user.person,
                    )
                    action_type = ActivityType.ADD_COMMENT
                else:
                    comment = get_object_or_404(Comment, pk=comment_id)
                    if comment.discussion_id != discussion_id:
                        return HttpResponseNotFound()
                    if not can_edit_comment(request.user, comment):
                        return HttpResponseForbidden()
                    comment.modified = timezone.now()
                    action_type = ActivityType.UPDATE_COMMENT
                    activity_details['old_content'] = comment.content

                comment.content = form.cleaned_data['content']
                comment.save()

                activity_details['content'] = comment.content
                activity = Activity.objects.create(
                    actor=request.user.person,
                    action_type=action_type,
                    object_link=ActivityLink.for_comment(comment),
                    target_link=activity_target,
                    details=activity_details,
                )
            if on_success:
                on_success(activity)
            messages.add_message(request, messages.SUCCESS, 'Komentár uložený')
            return HttpResponseRedirect(success_url)
    else:
        if comment_id:
            comment = get_object_or_404(Comment, pk=comment_id)
            if comment.discussion_id != discussion_id:
                return HttpResponseNotFound()
            form = CommentForm(initial={'content': comment.content, 'parent': comment.parent_id})
        else:
            form = CommentForm()

    return render(request, 'comments/edit.html', {
        'form': form,
        'comment_id': comment_id,
    })


@login_required
def events_edit_review(request, event_id, event_slug):
    event = get_object_or_404(Event, pk=event_id, slug=event_slug)

    try:
        review = event.review
        action_type = ActivityType.UPDATE_REVIEW
    except Review.DoesNotExist:
        review = Review(event=event)
        action_type = ActivityType.ADD_REVIEW

    if request.method == 'POST':
        form = ReviewForm(request.POST, instance=review)
        if form.is_valid():
            with transaction.atomic():
                form.save()
                Activity.objects.create(
                    actor=request.user.person,
                    action_type=action_type,
                    target_link=ActivityLink.for_event(event)
                )
            messages.add_message(request, messages.SUCCESS, 'Záznam z akcie uložený')
            return HttpResponseRedirect(event.get_absolute_url())
    else:
        form = ReviewForm(instance=review)

    return render(request, 'events/edit-review.html', {
        'form': form,
    })


def events_photos(request, event_id, event_slug):
    event = get_object_or_404(Event.objects.prefetch_related('photos'), pk=event_id, slug=event_slug)

    if can_upload_to_gallery(request.user, event):
        photo = Photo(event=event, uploaded_by=request.user)

        if request.method == 'POST':
            if not can_upload_to_gallery(request.user, event):
                return HttpResponseForbidden()

            form = PhotoForm(request.POST, request.FILES, instance=photo)
            if form.is_valid():
                with transaction.atomic():
                    form.save()
                    Activity.objects.create(
                        actor=request.user.person,
                        action_type=ActivityType.ADD_PHOTO,
                        object_link=ActivityLink.for_photo(photo),
                        target_link=ActivityLink.for_event(event),
                    )
                messages.add_message(request, messages.SUCCESS, 'Fotka pridaná')
                return HttpResponseRedirect(reverse('events_photos', args=(event.id, event.slug)))
        else:
            form = PhotoForm(instance=photo)
    else:
        form = None

    return render(request, 'events/photos.html', {
        'upload_form': form,
        'event': event,
    })


@login_required
def topics_index(request):
    topics = Topic.objects.all()\
        .prefetch_related('author')\
        .annotate(comment_count=Count('discussion__comments'))\
        .order_by('created')
    return render(request, 'topics/list.html', {
        'topics': topics,
    })


@login_required
def topics_edit(request, topic_id=None, topic_slug=None):
    if topic_id is not None:
        topic = get_object_or_404(Topic, pk=topic_id, slug=topic_slug)
        if not can_edit_topic(request.user, topic):
            return HttpResponseForbidden()
    else:
        topic = Topic()

    if request.method == 'POST':
        old_topic = deepcopy(topic)
        form = TopicForm(request.POST, instance=topic)
        if form.is_valid():
            with transaction.atomic():
                new_topic = form.save(commit=False)
                if topic_id is None:
                    # When adding new topic, we need to create a discussion for it and set some fields
                    discussion = Discussion()
                    discussion.save()
                    new_topic.discussion = discussion
                    new_topic.author = request.user.person
                    new_topic.slug = slugify(new_topic.title)
                    action_type = ActivityType.ADD_TOPIC
                else:
                    action_type = ActivityType.UPDATE_TOPIC

                new_topic.save()
                form.save_m2m()
                activity_details = {}
                if topic_id is not None:
                    if old_topic.title != topic.title:
                        activity_details['old_title'] = old_topic.title
                        activity_details['title'] = topic.title
                    if old_topic.description != topic.description:
                        activity_details['old_content'] = old_topic.description
                        activity_details['content'] = topic.description
                else:
                    activity_details['title'] = topic.title
                    activity_details['content'] = topic.description
                activity = Activity.objects.create(
                    actor=request.user.person,
                    action_type=action_type,
                    object_link=ActivityLink.for_topic(new_topic),
                    details=activity_details
                )
            recipients = topic_notification_recipients(topic)
            send_topic_notification(request, topic, activity, recipients)
            messages.add_message(request, messages.SUCCESS, 'Téma uložená')
            return HttpResponseRedirect(new_topic.get_absolute_url())
    else:
        form = TopicForm(instance=topic)

    return render(request, 'topics/edit.html', {
        'form': form,
        'topic_exists': topic_id is not None,
    })


@login_required
def topics_detail(request, topic_id, topic_slug):
    topic = get_object_or_404(Topic, pk=topic_id, slug=topic_slug)

    def add_edit_url(comment):
        if request.user.is_authenticated and request.user.id == comment.author.id:
            comment.edit_url = reverse('topics_edit_comment', args=(topic.id, topic.slug, comment.id))

    return render(request, 'topics/detail.html', {
        'topic': topic,
        'add_comment_url': reverse('topics_add_comment', args=(topic.id, topic.slug)),
        'comments': Comment.objects.discussion_tree(topic.discussion_id, per_comment=add_edit_url),
        'can_edit': can_edit_topic(request.user, topic)
    })


def topics_edit_comment(request, topic_id, topic_slug, comment_id=None):
    topic = get_object_or_404(Topic, pk=topic_id, slug=topic_slug)

    def on_success(activity):
        recipients = topic_notification_recipients(topic)
        send_topic_notification(request, topic, activity, recipients)

    return edit_comment(request,
                        discussion_id=topic.discussion_id,
                        success_url=topic.get_absolute_url(),
                        comment_id=comment_id,
                        activity_target=ActivityLink.for_topic(topic),
                        on_success=on_success)


@login_required
def members_index(request):
    members = get_user_model().objects.filter(is_active=True).select_related('person').order_by('username').all()
    return render(request, 'members/list.html', {
        'members': members,
    })


@login_required
def members_detail(request, username):
    member = get_object_or_404(get_user_model().objects.select_related('person'), username=username, is_active=True)
    return render(request, 'members/detail.html', {
        'member': member,
        'can_edit': can_edit_user(request.user, member),
        'is_self': member.id == request.user.id,
        'can_deactivate': can_deactivate(request.user, member),
    })


@login_required
def members_edit(request, username):
    member = get_object_or_404(get_user_model().objects.select_related('person'), username=username, is_active=True)
    if not can_edit_user(request.user, member):
        return HttpResponseForbidden()

    if request.method == 'POST':
        form = ProfileForm(request.POST, request.FILES)
        if form.is_valid():
            with transaction.atomic():
                member.username = form.cleaned_data['username']
                member.email = form.cleaned_data['email']
                member.new_event_notifications = form.cleaned_data['new_event_notifications']
                member.event_notification_level = form.cleaned_data['event_notification_level']
                member.topic_notifications = form.cleaned_data['topic_notifications']
                member.person.nickname = form.cleaned_data['username']
                member.person.sex = form.cleaned_data['sex']
                avatar_action = None
                if form.cleaned_data['avatar']:
                    if not member.person.avatar:
                        avatar_action = ActivityType.ADD_PHOTO
                    else:
                        avatar_action = ActivityType.UPDATE_PHOTO
                    member.person.avatar = form.cleaned_data['avatar']
                member.person.save()
                member.save()
                # TODO: add generic update activity?
                if avatar_action is not None:
                    Activity.objects.create(
                        actor=request.user.person,
                        action_type=avatar_action,
                        target_link=ActivityLink.for_person(member.person)
                    )
            messages.add_message(request, messages.SUCCESS, 'Profil uložený')
            return HttpResponseRedirect(reverse('members_detail', args=(member.username,)))
    else:
        form = ProfileForm(initial={
            'username': member.username,
            'email': member.email,
            'new_event_notifications': member.new_event_notifications,
            'event_notification_level': member.event_notification_level,
            'topic_notifications': member.topic_notifications,
            'sex': member.person.sex,
        })

    return render(request, 'members/edit.html', {
        'member': member,
        'form': form,
    })


@login_required
def members_deactivate(request, username):
    member = get_object_or_404(get_user_model().objects.select_related('person'), username=username, is_active=True)

    if not can_deactivate(request.user, member):
        return HttpResponseForbidden()

    if request.method == 'POST':
        form = DeactivateForm(request.POST)
        if form.is_valid():
            with transaction.atomic():
                member.is_active = False
                member.save()
                Activity.objects.create(
                    actor=request.user.person,
                    action_type=ActivityType.DEACTIVATE_USER,
                    object_link=ActivityLink.for_person(member.person)
                )
            messages.add_message(request, messages.SUCCESS, 'Používateľ deaktivovaný')
            return HttpResponseRedirect(reverse('members_index'))
    else:
        form = DeactivateForm()

    return render(request, 'members/deactivate.html', {
        'member': member,
        'form': form,
    })


@login_required
def persons_select2_data(request):
    """Return a list of persons for select2 query.

    See https://select2.org/data-sources/ajax
    See https://select2.org/data-sources/formats
    """
    persons = Person.objects.order_by('nickname').all()

    term = request.GET.get('term')
    if term:
        persons = persons.filter(nickname__icontains=term)

    results = [{
        'id': person.id,
        'text': person.nickname,
        'avatar_thumbnail': person.avatar_thumbnail.url if person.avatar_thumbnail else None,
        'avatar_color': person.default_avatar_color(),
        'avatar_initials': person.default_avatar_initials(),
    } for person in persons]

    return JsonResponse({'results': results})


@login_required
def activities_index(request):
    all_activities = Activity.objects.select_related(
        'actor', 'actor__user',
        'object_link', 'object_link__event', 'object_link__person', 'object_link__topic',
        'target_link', 'target_link__event', 'target_link__person', 'target_link__topic').order_by('-time').all()
    paginator = Paginator(all_activities, 25)
    page = request.GET.get('page', "1")
    activities = paginator.page(page)
    return render(request, 'activities/list.html', {
        'activities': activities,
    })


def blog_index(request):
    articles = BlogArticle.objects.order_by('-created').all()
    for article in articles:
        article.can_edit = can_edit_blog(request.user, article)
    return render(request, 'blog/list.html', {
        'articles': articles,
        'can_add': can_create_blog(request.user),
    })


@login_required
def blog_edit(request, article_id=None):
    if article_id is not None:
        article = get_object_or_404(BlogArticle, pk=article_id)
        if not can_edit_blog(request.user, article):
            return HttpResponseForbidden()
    else:
        article = BlogArticle()

    if request.method == 'POST':
        old_article = deepcopy(article)
        form = BlogArticleForm(request.POST, instance=article)
        if form.is_valid():
            with transaction.atomic():
                new_article = form.save(commit=False)
                if article_id is None:
                    # When adding new article, we need to set some fields
                    new_article.author = request.user.person
                    action_type = ActivityType.ADD_BLOG_ARTICLE
                else:
                    action_type = ActivityType.UPDATE_BLOG_ARTICLE
                    new_article.modified = timezone.now()

                new_article.save()
                form.save_m2m()
                activity_details = {}
                if article_id is not None:
                    if old_article.title != article.title:
                        activity_details['old_title'] = old_article.title
                        activity_details['title'] = article.title
                    if old_article.content != article.content:
                        activity_details['old_content'] = old_article.content
                        activity_details['content'] = article.content
                else:
                    activity_details['title'] = article.title
                    activity_details['content'] = article.content
                Activity.objects.create(
                    actor=request.user.person,
                    action_type=action_type,
                    object_link=ActivityLink.for_blog_article(new_article),
                    details=activity_details
                )
            messages.add_message(request, messages.SUCCESS, 'Novinka uložená')
            return HttpResponseRedirect(reverse('blog_index'))
    else:
        form = BlogArticleForm(instance=article)

    return render(request, 'blog/edit.html', {
        'form': form,
        'article_exists': article_id is not None,
    })


def material_index(request):
    models = (MaterialModel.objects.select_related('category')
              .order_by('category__order', 'category__name', 'name')
              .annotate(item_count=Count('items', filter=Q(items__decommissioned_date__isnull=True)))
              .filter(item_count__gt=0)
              .all())
    return render(request, 'material/list.html', {
        'models': models,
        'can_add': can_create_material(request.user),
        'all_link': True,
        'can_borrow': can_borrow_material(request.user),
    })


def material_index_all(request):
    models = (MaterialModel.objects.select_related('category')
              .order_by('category__order', 'category__name', 'name')
              .annotate(item_count=Count('items', filter=Q(items__decommissioned_date__isnull=True)))
              .all())
    return render(request, 'material/list.html', {
        'models': models,
        'can_add': can_create_material(request.user),
        'all_link': False,
        'can_borrow': can_borrow_material(request.user),
    })


def material_detail(request, material_model_id):
    model = get_object_or_404(MaterialModel, pk=material_model_id)
    commission_form = MaterialCommissionForm(initial={'commission_count': 0})
    decommission_form = MaterialDecommissionForm(initial={'decommission_date': timezone.now().date()})
    return render(request, 'material/detail.html', {
        'model': model,
        'can_edit': can_edit_material(request.user, model),
        'commission_form': commission_form,
        'decommission_form': decommission_form,
        'can_borrow': can_borrow_material(request.user),
    })


@login_required
def material_commission(request, material_model_id):
    model = get_object_or_404(MaterialModel.objects.select_related('category'), pk=material_model_id)

    if not can_edit_material(request.user, model):
        return HttpResponseForbidden()

    decommission_form = MaterialDecommissionForm(initial={'decommission_date': timezone.now().date()})

    if request.method == 'POST':
        commission_form = MaterialCommissionForm(request.POST)
        if commission_form.is_valid():
            if commission_material(commission_form, model, request.user.person):
                messages.add_message(request, messages.SUCCESS, 'Materiál zaradený do klubovne')
                return HttpResponseRedirect(reverse('material_detail', args=(model.id,)))
    else:
        commission_form = MaterialCommissionForm(initial={'commission_count': 0})

    return render(request, 'material/detail.html', {
        'model': model,
        'can_edit': True,
        'commission_form': commission_form,
        'decommission_form': decommission_form,
        'can_borrow': can_borrow_material(request.user),
    })


@login_required
def material_decommission(request, material_model_id):
    model = get_object_or_404(MaterialModel.objects.select_related('category'), pk=material_model_id)

    if not can_edit_material(request.user, model):
        return HttpResponseForbidden()

    commission_form = MaterialCommissionForm(initial={'commission_count': 0})
    item_ids = []

    if request.method == 'POST':
        decommission_form = MaterialDecommissionForm(request.POST)
        if decommission_form.is_valid():
            with transaction.atomic():
                item_ids = request.POST.getlist('item_id')
                try:
                    item_ids = [int(x) for x in item_ids]
                except ValueError:
                    decommission_form.add_error(error='Chyba pri overovaní ID')
                else:
                    updated_count = MaterialItem.objects.filter(model=model, id__in=item_ids)\
                        .update(decommissioned_date=decommission_form.cleaned_data['decommission_date'])

                    if updated_count == 0:
                        decommission_form.add_error(error='Nenašli sa žiadne ID')
                    else:
                        Activity.objects.create(
                            actor=request.user.person,
                            action_type=ActivityType.DECOMMISSION_MATERIAL_ITEM,
                            target_link=ActivityLink.for_material_model(model),
                            details={
                                'count': updated_count,
                                'decommissioned_date': decommission_form.cleaned_data['decommission_date'].isoformat(),
                            }
                        )

                        messages.add_message(request, messages.SUCCESS, 'Materiál vyradený z klubovne')
                        return HttpResponseRedirect(reverse('material_detail', args=(model.id,)))
    else:
        decommission_form = MaterialDecommissionForm(initial={'commission_count': 0})

    return render(request, 'material/detail.html', {
        'model': model,
        'can_edit': True,
        'commission_form': commission_form,
        'decommission_form': decommission_form,
        'selected_items': item_ids,
        'can_borrow': can_borrow_material(request.user),
    })


@login_required
def material_edit(request, material_model_id=None):
    if material_model_id is not None:
        model = get_object_or_404(MaterialModel, pk=material_model_id)
        if not can_edit_material(request.user, model):
            return HttpResponseForbidden()
    else:
        if not can_create_material(request.user):
            return HttpResponseForbidden()
        model = MaterialModel()

    if request.method == 'POST':
        old_model = deepcopy(model)
        form = MaterialModelForm(request.POST, instance=model)
        commission_form = MaterialCommissionForm(request.POST)
        if form.is_valid() and commission_form.is_valid():
            with transaction.atomic():
                new_model = form.save()

                activity_details = {}
                if material_model_id is not None:
                    action_type = ActivityType.UPDATE_MATERIAL_MODEL
                    if old_model.name != model.name:
                        activity_details['old_name'] = old_model.name
                        activity_details['name'] = new_model.name
                    if old_model.description != new_model.description:
                        activity_details['old_content'] = old_model.description
                        activity_details['content'] = new_model.description
                    if old_model.category != new_model.category:
                        activity_details['old_category'] = {
                            'id': old_model.category.id,
                            'name': old_model.category.name,
                        }
                        activity_details['category'] = {
                            'id': new_model.category.id,
                            'name': new_model.category.name,
                        }
                else:
                    action_type = ActivityType.ADD_MATERIAL_MODEL
                    activity_details['name'] = new_model.name
                    activity_details['content'] = new_model.description
                    activity_details['category'] = {
                        'id': new_model.category.id,
                        'name': new_model.category.name,
                    }
                Activity.objects.create(
                    actor=request.user.person,
                    action_type=action_type,
                    object_link=ActivityLink.for_material_model(model),
                    details=activity_details
                )

                commission_material(commission_form, new_model, request.user.person)
            messages.add_message(request, messages.SUCCESS, 'Materiál uložený')
            return HttpResponseRedirect(reverse('material_detail', args=(new_model.id,)))
    else:
        form = MaterialModelForm(instance=model)
        commission_form = MaterialCommissionForm(initial={'commission_count': 0})

    return render(request, 'material/edit.html', {
        'form': form,
        'model_exists': material_model_id is not None,
        'commission_form': commission_form,
    })


def commission_material(commission_form, model, actor):

    commission_count = commission_form.cleaned_data['commission_count']
    if commission_count > 0:
        with transaction.atomic():
            date = commission_form.cleaned_data['commission_date']

            for i in range(commission_count):
                item = MaterialItem()
                item.commissioned_date = date
                item.model = model
                item.save()

            Activity.objects.create(
                actor=actor,
                action_type=ActivityType.COMMISSION_MATERIAL_ITEM,
                target_link=ActivityLink.for_material_model(model),
                details={
                    'count': commission_count,
                    'commissioned_date': None if date is None else date.isoformat(),
                }
            )

        return True
    return False


@login_required
def material_cart(request):
    cart = MaterialCart(request.session)

    if request.method == 'POST':
        formset = cart.as_formset(request.POST)
        loan_form = MaterialCartLoanForm(request.POST, use_required_attribute=False)

        if formset.is_valid():
            cart.update_from_formset(formset)

            if request.POST.get('cart_action') == 'update_borrow':
                if loan_form.is_valid():
                    try:
                        with transaction.atomic():
                            models_counts = [(item.model, item.count) for item in cart.contents()]
                            items = MaterialLoan.objects.lend_models(
                                models_counts,
                                loan_form.cleaned_data['during'],
                                request.user.person)
                            Activity.objects.create(
                                actor=request.user.person,
                                action_type=ActivityType.BORROW_MATERIAL_ITEMS,
                                object_link=ActivityLink.for_material_items(items),
                                details={
                                    'material_summary': material_summary(models_counts),
                                }
                            )
                    except MaterialNotAvailable as e:
                        for form in formset:
                            if form.is_valid() and 'model' in form.cleaned_data:
                                model_id = form.cleaned_data['model'].pk
                                if model_id in e.material_count:
                                    form.add_error('count', ValidationError(
                                        'Nedostatok materiálu v klubovni, chýba %(count)s položiek',
                                        code='invalid',
                                        params={'count': e.material_count[model_id]},
                                    ))
                        messages.add_message(request, messages.ERROR, 'Materiál sa nepodarilo zarezervovať')
                    else:
                        cart.reset()
                        cart.save()
                        messages.add_message(request, messages.SUCCESS, 'Materiál zarezervovaný')
                        return HttpResponseRedirect(reverse('material_cart'))
            else:
                messages.add_message(request, messages.SUCCESS, 'Košík upravený')
                cart.save()
                return HttpResponseRedirect(reverse('material_cart'))
    else:
        formset = cart.as_formset()
        loan_form = MaterialCartLoanForm(use_required_attribute=False)

    return render(request, 'material/cart.html', {
        'cart': cart,
        'formset': formset,
        'loan_form': loan_form,
    })


@login_required
def material_add_to_cart(request):
    cart = MaterialCart(request.session)

    if request.method == 'POST':
        add_form = MaterialCartItemForm(request.POST)
        if add_form.is_valid():
            cart.add_model(add_form.cleaned_data['model'], add_form.cleaned_data['count'])
            cart.save()
            messages.add_message(request, messages.SUCCESS, 'Materiál pridaný do košíka')
            return HttpResponseRedirect(reverse('material_cart'))
    else:
        add_form = MaterialCartItemForm()

    return render(request, 'material/cart.html', {
        'cart': cart,
        'add_form': add_form,
    })


def material_summary(models_counts):
    summary = []
    for model, count in models_counts:
        summary.append({
            'model_id': model.id,
            'model_name': model.name,
            'count': count,
        })
    return summary


@login_required
def material_my_loans(request):
    models = MaterialModel.objects\
        .filter(items__materialloan__loaned_to=request.user.person, items__materialloan__returned_time__isnull=True)\
        .annotate(item_count=Count('items'), during=F('items__materialloan__during'))\
        .order_by('during', 'name')
    initial_values = [{
            'model': item,
            'during': item.during,
            'count': item.item_count,
        } for item in models]

    if request.method == 'POST':
        formset = MaterialReturnFormset(request.POST, initial=initial_values)
        if formset.is_valid():
            try:
                with transaction.atomic():
                    return_items = []
                    model_by_id = {}
                    count_by_id = collections.Counter()

                    for form in formset:
                        if form.is_valid():
                            model = form.cleaned_data['model']
                            return_items.append((model, form.cleaned_data['during'], form.cleaned_data['count']))
                            model_by_id[model.id] = model
                            count_by_id[model.id] += form.cleaned_data['count']

                    items = MaterialLoan.objects.return_models(
                        return_items,
                        request.user.person)

                    Activity.objects.create(
                        actor=request.user.person,
                        action_type=ActivityType.RETURN_MATERIAL_ITEMS,
                        object_link=ActivityLink.for_material_items(items),
                        details={
                            'material_summary': material_summary([(model_by_id[model_id], count_by_id[model_id])
                                                                  for model_id in model_by_id
                                                                  if count_by_id[model_id] > 0])
                        }
                    )
            except LoansNotAvailable as e:
                for form in formset:
                    if form.is_valid():
                        model_id = form.cleaned_data['model'].pk
                        during = form.cleaned_data['during']
                        key = (model_id, date_range_exclusive(during))
                        if key in e.loan_count:
                            form.add_error('count', ValidationError(
                                'Snažíš sa vrátiť o %(count)s položiek viac ako máš požičané',
                                code='invalid',
                                params={'count': e.loan_count[key]},
                            ))
                messages.add_message(request, messages.ERROR, 'Materiál sa nepodarilo vrátiť')
            except NothingToReturn:
                messages.add_message(request, messages.ERROR, 'Nebol vybratý žiaden materiál na vrátenie')
            else:
                messages.add_message(request, messages.SUCCESS, 'Materiál vrátený')
                return HttpResponseRedirect(reverse('material_my_loans'))
    else:
        formset = MaterialReturnFormset(initial=initial_values)

    return render(request, 'material/my-loans.html', {
        'formset': formset,
    })


@login_required
def material_loans(request):
    models = MaterialModel.objects\
        .filter(items__materialloan__returned_time__isnull=True, items__materialloan__isnull=False)\
        .annotate(item_count=Count('items__materialloan'),
                  during=F('items__materialloan__during'),
                  loaned_to_nickname=F('items__materialloan__loaned_to__nickname'))\
        .order_by('during', 'name')

    return render(request, 'material/loans.html', {
        'models': models,
    })


def pages_onas(request):
    articles = BlogArticle.objects.order_by('-created').all()[:3]
    for article in articles:
        article.can_edit = can_edit_blog(request.user, article)
    return render(request, 'pages/o-nas.html', {
        'articles': articles,
    })


@login_required
def redirect_old_comment_view(request, comment_id):
    comment = get_object_or_404(Comment, pk=comment_id)
    if comment.parent_id is not None:
        # subcomments did not have their own page
        return HttpResponseNotFound()

    try:
        topic = comment.discussion.topic
        return HttpResponseRedirect(reverse('topics_edit_comment', args=(topic.id, topic.slug, comment_id)))
    except Topic.DoesNotExist:
        event = comment.discussion.event
        return HttpResponseRedirect(reverse('events_edit_comment', args=(event.id, event.slug, comment_id)))


class RedirectWithSlugView(RedirectView):
    model = None
    slug_field = 'slug'
    slug_url_kwarg = 'slug'
    pk_url_kwarg = 'pk'

    def get_redirect_url(self, *args, **kwargs):
        pk = kwargs[self.pk_url_kwarg]
        queryset = self.model.objects.all().values_list(self.slug_field, flat=True)
        try:
            slug = queryset.get(pk=pk)
        except queryset.model.DoesNotExist:
            raise Http404('No %s matches the given query.' % queryset.model._meta.object_name)
        kwargs[self.slug_url_kwarg] = slug
        return super().get_redirect_url(*args, **kwargs)


class OldEventRedirectView(RedirectWithSlugView):
    model = Event
    pk_url_kwarg = 'event_id'
    slug_url_kwarg = 'event_slug'


class OldTopicRedirectView(RedirectWithSlugView):
    model = Topic
    pk_url_kwarg = 'topic_id'
    slug_url_kwarg = 'topic_slug'
