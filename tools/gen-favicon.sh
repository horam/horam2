#!/usr/bin/env bash

for size in 16 32 48 96 128;
do
    inkscape -z -e horam/web/static/favicon-$size.png -w $size -h $size horam/web/static/favicon.svg
done