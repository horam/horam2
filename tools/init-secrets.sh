#!/bin/bash

secrets_dir=.dev-secrets

function gen_secret {
    secret_file="$secrets_dir/$1"
    if [ ! -f "$secret_file" ];
    then
        dd if=/dev/urandom bs=40 count=1 | od -A n -t x1 -w40 | tr -d ' \n' >"$secret_file"
    fi
}

mkdir -p "$secrets_dir"
gen_secret secret-key
gen_secret postgres-password
