This directory contains a vendored copy of django-user-sessions package.

It is here because pipenv did not respect locked Django version when
installing this package via git.

https://github.com/martin-sucha/django-user-sessions
https://github.com/lukegb/django-user-sessions
https://github.com/Bouke/django-user-sessions